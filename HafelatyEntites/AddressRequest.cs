namespace HafelatyEntites
{
    public class AddressRequest
    {
        public int Id { get; set; }

        public int ChildrenId { get; set; }

        public float StartLat { get; set; }

        public float StartLong { get; set; }

        public string StartAddress { get; set; }

        public float EndLat { get; set; }

        public float EndLong { get; set; }

        public string EndAddress { get; set;}
    }
}