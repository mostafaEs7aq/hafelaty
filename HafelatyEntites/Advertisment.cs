namespace HafelatyEntites
{
    public class Advertisment
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Descreption { get; set; }

        public string Url { get; set; }

        public int IsDeleted { get; set; }
    }
}