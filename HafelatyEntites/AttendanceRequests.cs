namespace HafelatyEntites
{
    public class AttendanceRequests
    {
        public int Id { get; set; }

        public int Status { get; set; }

        public int ChildrensId { get; set; }

        public int ActionTypeId { get; set; }
    }
}