using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HafelatyEntites
{
    public class Bus
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("User")]
        public int DriverId { get; set; }

        public int Capacity { get; set; }

        public int CurrentAmount { get; set; }

        public string Area { get; set; }

        public string Model { get; set; }

        public string Plate { get; set; }

        public string Licenses { get; set; }

        public int Space { get; set; }

        public int IsDeleted { get; set; }

        public string Status { get; set; }

        public int Trip { get; set; }

        public User User { get; set; }

        public ICollection<Passengers> Passengers { get; set; }

        public List<BusRound> BusRounds { get; set; }

    }
}