using System.Collections.Generic;

namespace HafelatyEntites
{
    public class BusRound
    {
        public int Id { get; set; }

        public string TripName { get; set; }

        public Bus Bus { get; set; }

        public int BusId { get; set; }

        public RoundTimes RoundTimes { get; set; }

        public int RoundTimesId { get; set; }

        public RoundTypes RoundTypes { get; set; }

        public int RoundTypesId { get; set; }

        public List<RoundPassengers> RoundPassengers { get; set; }

        public int Capacity { get; set; }

        public int CurrentAmount { get; set; }

        public int IsDeleted { get; set; }
    }
}