using System.Collections.Generic;

namespace HafelatyEntites
{
    public class Case
    {
        public int Id { get; set; }

        public string CaseName { get; set; }

        public List<ChildrensCases> ChildrensCases { get; set; }
    }
}