using System;
using System.Collections.Generic;

namespace HafelatyEntites
{
    public class Childrens
    {
        public int Id { get; set; }

        public string Firstname { get; set; }

        public string Middlename { get; set; }

        public string Lastname { get; set; }

        public string Age { get; set; }

        public float? StartLat { get; set; }

        public float? StartLong { get; set; }

        public string StartAddress { get; set; }

        public float? EndLat { get; set; }

        public float? EndLong { get; set; }

        public string EndAddress { get; set; }

        public int IsDeleted { get; set; }

        public Parents Parents { get; set; }

        public int ParentsId { get; set; }

        public int SchoolId { get; set; }

        // public ICollection<Photo> Photos { get; set; }

        public Passengers Passengers { get; set; }

        public ChildrensCases ChildrensCases { get; set; }

        public List<RoundPassengers> RoundPassengers { get; set; }

        


    }
}