namespace HafelatyEntites
{
    public class ChildrensCases
    {
        public int Id { get; set; }
        public int CaseId { get; set; }
        public Case Case { get; set; }
        public int ChildrenId { get; set; }
        public Childrens Children { get; set; }
    }
}