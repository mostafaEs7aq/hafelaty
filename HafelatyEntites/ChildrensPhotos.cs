using System;

namespace HafelatyEntites
{
    public class ChildrensPhotos
    {
        public int Id { get; set; }

        public string Url { get; set; }

        public DateTime DateAdded { get; set; }

        public Childrens Childrens { get; set; }

    }
}