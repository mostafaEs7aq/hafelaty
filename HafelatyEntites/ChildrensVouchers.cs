using System;

namespace HafelatyEntites
{
    public class ChildrensVouchers
    {
        public int Id { get; set; }
        public int ChildrenId { get; set; }
        public int VoucherCode { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime ExpireDate { get; set; }
    }
}