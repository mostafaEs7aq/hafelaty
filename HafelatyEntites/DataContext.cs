﻿using Microsoft.EntityFrameworkCore;
using System;

namespace HafelatyEntites
{
    public class DataContext : DbContext
    {
        public DbSet<UserType> UserType { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Gender> Gender { get; set; }
        public DbSet<UserInfo> UserInfo { get; set; }
        public DbSet<Schools> Schools { get; set; }
        public DbSet<Parents> Parents { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<Verification> Verification { get; set; }
        public DbSet<Childrens> Childrens { get; set; }
        public DbSet<ChildrensPhotos> ChildrensPhotos { get; set; }
        public DbSet<Bus> Bus { get; set; }
        public DbSet<Passengers> Passengers { get; set; }
        public DbSet<Advertisment> Advertisments { get; set; }
        public DbSet<Offers> HotOffers { get; set; }
        public DbSet<ActionType> Actions { get; set; }
        public DbSet<AttendanceRequests> AttendanceRequests { get; set; }
        public DbSet<AddressRequest> AddressRequest { get; set; }
        public DbSet<Emergency> Emergencies { get; set; }
        public DbSet<EmergencyRequest> EmergencyRequests { get; set; }
        public DbSet<DriverBehaviour> DriverBehaviours { get; set; }
        public DbSet<Case> Cases { get; set; }
        public DbSet<ChildrensCases> ChildrensCases { get; set; }
        public DbSet<RoundTimes> RoundTimes { get; set; }
        public DbSet<RoundTypes> RoundTypes { get; set; }
        public DbSet<BusRound> BusRounds { get; set; }
        public DbSet<RoundPassengers> RoundPassengers { get; set; }
        public DbSet<ChildrensVouchers> ChildrensVouchers { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Schools>()
                .Property(b => b.IsDeleted)
                .HasDefaultValue(0);

            modelBuilder.Entity<BusRound>()
                .Property(b => b.CurrentAmount)
                .HasDefaultValue(0);

            modelBuilder.Entity<Verification>()
                .Property(b => b.Used)
                .HasDefaultValue(0);

            modelBuilder.Entity<BusRound>()
                .Property(b => b.IsDeleted)
                .HasDefaultValue(0);

            modelBuilder.Entity<Passengers>()
                .Property(b => b.Attendance)
                .HasDefaultValue(0);

            // modelBuilder.Entity<Passengers>()
            //     .Property(b => b.ActionTypeId)
            //     .HasDefaultValue(1);

            modelBuilder.Entity<Childrens>()
                .Property(b => b.IsDeleted)
                .HasDefaultValue(0);

            modelBuilder.Entity<Advertisment>()
                .Property(b => b.IsDeleted)
                .HasDefaultValue(0);

            modelBuilder.Entity<Offers>()
                .Property(b => b.IsDeleted)
                .HasDefaultValue(0);


            modelBuilder.Entity<Bus>()
                .Property(b => b.IsDeleted)
                .HasDefaultValue(0);

            modelBuilder.Entity<User>()
                .Property(b => b.IsDeleted)
                .HasDefaultValue(0);

            modelBuilder.Entity<Bus>()
                .Property(b => b.Trip)
                .HasDefaultValue(1);

            modelBuilder.Entity<User>()
                .Property(b => b.IsLogginedIn)
                .HasDefaultValue(0);

            modelBuilder.Entity<RoundPassengers>()
                .Property(b => b.Attendance)
                .HasDefaultValue(0);

            modelBuilder.Entity<Bus>()
                .Property(b => b.Status)
                .HasDefaultValue("Off");


            modelBuilder.Entity<Bus>()
                    .Property(b => b.CurrentAmount)
                    .HasDefaultValue(0);

            modelBuilder.Entity<Bus>(entity => {
                entity.HasKey(z => z.Id);
                entity.HasOne(p => p.User)
                        .WithOne(b => b.Bus)
                        .HasForeignKey<Bus>(b => b.DriverId);
            });

            modelBuilder.Entity<Passengers>(entity => {
                entity.HasKey(z => z.Id);
                entity.HasOne(p => p.Childrens)
                        .WithOne(b => b.Passengers)
                        .HasForeignKey<Passengers>(b => b.ChildrenId);
            });

            modelBuilder.Entity<Passengers>(entity => {
                entity.HasKey(z => z.Id);
                entity.HasOne(p => p.Childrens)
                        .WithOne(b => b.Passengers)
                        .HasForeignKey<Passengers>(b => b.ChildrenId);
            });



        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=.;Initial Catalog=HafelatyNewVersion;User ID=sa;Password=P@ssw0rd;");
            // optionsBuilder.UseSqlServer(@"Data Source=198.38.83.200;Initial Catalog=hafelaty_db;User ID=hafelaty_sa;Password=P@ssw0rd;");
        }


    }
}
