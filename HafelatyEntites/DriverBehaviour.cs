using System;

namespace HafelatyEntites
{
    public class DriverBehaviour
    {
        public int Id { get; set; }

        public int DriverId { get; set; }

        public DateTime ActionTime { get; set; }
    }
}