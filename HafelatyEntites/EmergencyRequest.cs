namespace HafelatyEntites
{
    public class EmergencyRequest
    {
        public int Id { get; set; }

        public int DriverId { get; set; }

        public int EmergencyId { get; set; }
    }
}