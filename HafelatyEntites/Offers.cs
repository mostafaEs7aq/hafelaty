namespace HafelatyEntites
{
    public class Offers
    {
        public int Id { get; set; } 

        public string Url { get; set; }

        public string HyperLink { get; set; }
        
        public int IsDeleted { get; set; }
    }
}