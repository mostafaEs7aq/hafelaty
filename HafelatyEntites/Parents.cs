namespace HafelatyEntites
{
    public class Parents
    {
        public int Id { get; set; }

        public int PrimaryParent { get; set; }

        public int? fSecondaryParent { get; set; }

        public int? SSecondaryParet { get; set; }
    }
}