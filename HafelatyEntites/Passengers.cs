using System.ComponentModel.DataAnnotations.Schema;

namespace HafelatyEntites
{
    public class Passengers
    {
        public int Id { get; set; }

        [ForeignKey("Childrens")]
        public int ChildrenId { get; set; }

        [ForeignKey("Bus")]
        public int BusId { get; set; }

        // [ForeignKey("ActionType")]
        // public int ActionTypeId { get; set; }

        public Bus Bus { get; set; }

        public Childrens Childrens { get; set; }

        // public ActionType ActionType { get; set; }

        public int Attendance { get; set; }

    }
}