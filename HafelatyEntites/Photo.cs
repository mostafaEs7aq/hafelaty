using System;

namespace HafelatyEntites
{
    public class Photo
    {
        public int Id { get; set; }

        public string Url { get; set; }

        // public DateTime DateAdded { get; set; }

        public int ImageFor { get; set; }

        public string Type { get; set; }
    }
}