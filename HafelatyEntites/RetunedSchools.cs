namespace HafelatyEntites
{
    public class RetunedSchools
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public float Lat { get; set; }

        public float Lng { get; set; }

        public string Area { get; set; }

        public string Type { get; set; }
    }
}