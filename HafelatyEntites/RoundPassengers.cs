namespace HafelatyEntites
{
    public class RoundPassengers
    {
        public int Id { get; set; }

        public Childrens Children { get; set; }
        
        public int ChildrenId { get; set; }

        public BusRound BusRound { get; set; }

        public int BusRoundId { get; set; }

        public int Attendance { get; set; }
    }
}