using System.Collections.Generic;

namespace HafelatyEntites
{
    public class RoundTimes
    {
        public int Id { get; set; }

        public string Times { get; set; }

        public List<BusRound> BusRound { get; set; }
    }
}