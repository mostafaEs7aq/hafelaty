using System.Collections.Generic;

namespace HafelatyEntites
{
    public class RoundTypes
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public List<BusRound> BusRound { get; set; }
    }
}