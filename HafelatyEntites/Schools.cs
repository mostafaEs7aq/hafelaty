namespace HafelatyEntites
{
    public class Schools
    {
        public int Id { get; set; }

        public string SchoolName { get; set; }

        public string Address { get; set; }

        public string Phonenumber { get; set; }

        public float Lat { get; set; }

        public float Long { get; set; }
        
        public int IsDeleted { get; set; } // 0 means no and 1 means yes

        
    }
}