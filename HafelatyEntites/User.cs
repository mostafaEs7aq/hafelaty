using System.Collections.Generic;

namespace HafelatyEntites
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public string Email { get; set; }
        public string Phonenumber { get; set; }
        public int UserTypeId { get; set; }

        public int IsDeleted { get; set; }

        public string DeviceToken { get; set; }
        
        public int IsLogginedIn { get; set; }

        // public ICollection<Photo> Photos { get; set; }
        public ICollection<UserInfo> Info { get; set; }

        public UserType UserType { get; set; }

        public Bus Bus { get; set; }
    }
}