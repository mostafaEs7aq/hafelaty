using System;
using System.Collections.Generic;

namespace HafelatyEntites
{
    public class UserInfo
    {
        public int Id { get; set; }

        public string Firstname { get; set; }

        public string Middlename { get; set; }

        public string Lastname { get; set; }

        public int Age { get; set; }

        public DateTime Created { get; set; }

        public User User { get; set; }

        public int UserId { get; set; }

        public Gender Gender { get; set; }

        public int GenderId { get; set; }


    }
}