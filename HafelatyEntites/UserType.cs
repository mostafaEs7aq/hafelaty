namespace HafelatyEntites
{
    public class UserType
    {
        public int Id { get; set; }
        public string TypeName { get; set; }
    }
}