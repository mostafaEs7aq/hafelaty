namespace HafelatyEntites
{
    public class Verification
    {
        public int Id { get; set; }

        public int CodeHash { get; set; }

        public int Used { get; set; }
    }
}