﻿using HafelatyEntites;
using HafelatyServices.Dtos;
using HafelatyServices.Dtos.ToReturnDto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HafelatyServices
{
    public class AdminServices
    {
        #region Define as Singleton
        private static AdminServices _Instance;

        public static AdminServices Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new AdminServices();
                }

                return (_Instance);
            }
        }

        private AdminServices()
        {
        }
        #endregion


          public Schools AddSchool(Schools schoolDto)
         {
            DataContext _context = new DataContext();


            var schoolToAdd =  _context.Schools.Add(schoolDto);
                 _context.SaveChanges();

                return schoolDto;
            
         }

        public Schools EditSchool(SchoolToUpdateDto schoolDto)
        {
            DataContext _context = new DataContext();



            var schoolRecord = _context.Schools.FirstOrDefault(x => x.Id == schoolDto.Id);

                if (schoolRecord == null)
                    return null;

                schoolRecord.SchoolName = schoolDto.SchoolName;
                schoolRecord.Address = schoolDto.Address;
                schoolRecord.Phonenumber = schoolDto.Phonenumber;



                var schoolToUpdate = _context.Schools.Update(schoolRecord);
                 _context.SaveChanges();

                return schoolRecord;
            
        }

        public Schools DeleteSchool(int SchoolId)
        {
            DataContext _context = new DataContext();


            var schoolRecord = _context.Schools.FirstOrDefault(x => x.Id == SchoolId);

                if (schoolRecord == null)
                    return null;

                schoolRecord.IsDeleted = 1;

                var schoolToDelete = _context.Schools.Update(schoolRecord);
                 _context.SaveChanges();

                return schoolRecord;
            
        }

        public bool SchoolExists(string schoolname)
        {
            DataContext _context = new DataContext();


            if (_context.Schools.Any(x => x.SchoolName == schoolname))
                {
                    return true;
                }

                return false;
            


        }

        

        public bool CreateVouchers(CreateCardsDto createCardsDto)
        {
            DataContext _context = new DataContext();

            int totalAmount = createCardsDto.amount;
            string cardHash;


                Random random = new Random();
                int stringfiyRandom = random.Next();
                string randomDigit = stringfiyRandom.ToString();

                for(int i = 0; i < totalAmount; i++){

                    CreateCardHash(randomDigit, out cardHash);


                    var card = new Verification {   

                            CodeHash = Int32.Parse(cardHash)
                    };


                    var createdCard = _context.Verification.Add(card);
                     _context.SaveChanges();


                }

                return true;


        }

        private void CreateCardHash(string randomDigit, out string cardHash)
        {
                Random random = new Random();
                int convert = Int32.Parse(randomDigit);
                int stringfiyRandom = random.Next(convert);
                cardHash = stringfiyRandom.ToString();

        }

        public List<Verification> GetCards()
        {
            DataContext _context = new DataContext();

            var vouchers = _context.Verification.Where(x => x.Used == 0).ToList();

            return vouchers;
        }

        public Bus AddBus(Bus busToAddDto)
        {
            DataContext _context = new DataContext();

            int capacity = busToAddDto.Capacity;
                int currentAmount = busToAddDto.CurrentAmount;
                int spaceLeft;

                CalculateSpaceForBus(capacity, currentAmount, out  spaceLeft);

                busToAddDto.Space = spaceLeft;


                var busToAdd =  _context.Bus.Add(busToAddDto);
                 _context.SaveChanges();

                return busToAddDto;
        }

        public Bus EditBus(BusToUpdate busToUpdate)
        {

            DataContext _context = new DataContext();

            var bus = _context.Bus.FirstOrDefault(x => x.Id == busToUpdate.Id);
                if(bus == null)
                    return null;

            bus.DriverId = busToUpdate.DriverId;
            bus.Capacity = busToUpdate.Capacity;
            bus.Area = busToUpdate.Area;
            bus.Model = busToUpdate.Model;
            bus.Plate = busToUpdate.Plate;
            bus.Licenses = busToUpdate.Licenses;

            var editBus =  _context.Bus.Update(bus);
            _context.SaveChanges();

            return bus;



        }

        public Bus DeleteBus(BusToDeleteDto busToDeleteDto)
        {
            DataContext _context = new DataContext();

            var bus =  _context.Bus.FirstOrDefault(x => x.Id == busToDeleteDto.Id);
                if(bus == null)
                    return null;

                bus.IsDeleted = 1;
                var deleteBus =  _context.Bus.Update(bus);
                _context.SaveChanges();

                return bus;        
                
        }

        public List<Bus> GetAllBus()
        {
            DataContext _context = new DataContext();

            var buses = _context.Bus.Where(x => x.IsDeleted == 0).ToList();

            return buses;
        }

        public Bus GetBus(int id)
        {
            DataContext _context = new DataContext();

            var bus = _context.Bus.FirstOrDefault(x => x.IsDeleted == 0 && x.Id == id);
                if(bus == null)
                    return null;
            return bus;
        }

        public List<User> GetAvailableDrivers()
        {
            DataContext _context = new DataContext();

            var drivers = _context.Users.Where(x => x.UserTypeId == 3 && x.Bus == null && x.Info != null).ToList();
                    if(drivers == null)
                        return null;

    

                        
                return drivers;



        }

        public List<Childrens> GetAvailableChildrens()
        {
            DataContext _context = new DataContext();

            var childrens =  _context.Childrens.Where(x => x.IsDeleted == 0 && x.Passengers == null && x.EndAddress != null).ToList();
                    if(childrens == null)
                        return null;

                        
                return childrens;
        }



        private void CalculateSpaceForBus(int capacity, int currentAmount, out int spaceLeft)
        {

                spaceLeft = capacity - currentAmount;

        }


        // should be called when a child is added to a bus
        public bool UpdateBusAmounts(int BusId, int ChildId)
        {
            DataContext _context = new DataContext();

            var bus =  _context.Bus.FirstOrDefault(x => x.IsDeleted == 0 && x.Id == BusId);
                if(bus.CurrentAmount == bus.Capacity)
                    return false;

            if(_context.Passengers.Any(s => s.ChildrenId == ChildId))
                return false;

            int currentAmount = _context.Passengers.Count(x => x.BusId == BusId) + 1;

            CalculateSpaceForBus(bus.Capacity, currentAmount, out int spaceLeft);

            bus.CurrentAmount = currentAmount;
            bus.Space = spaceLeft;

            _context.Bus.Update(bus);
            _context.SaveChanges();

            return true;
        }

        public Passengers AddPassengers(Passengers passengersDto)
        {
            DataContext _context = new DataContext();


            var passenger = _context.Passengers.Add(passengersDto);
            _context.SaveChanges();

            return passengersDto;

        }

        public List<Bus> GetAvailableBuses()
        {
            DataContext _context = new DataContext();

            var buses = _context.Bus.Where(x => x.IsDeleted == 0 && x.Space != 0).ToList();
                    if(buses == null)
                        return null;

                        
                return buses;       
                
        }

        public Childrens getChildInfo(int Id)
        {
            DataContext _context = new DataContext();

            var children = _context.Childrens.FirstOrDefault(x => x.Id == Id);
                if(children == null)
                    return null;

            return children;
        }

        public ActionType getActionInfo(int Id)
        {
            DataContext _context = new DataContext();

            var action = _context.Actions.FirstOrDefault(x => x.Id == Id);
                if(action == null)
                    return null;

            return action;
        }

        public List<AddressRequest> GetAddressRequests()
        {
            DataContext _context = new DataContext();

            var requests =  _context.AddressRequest.ToList();
                if(!requests.Any())
                    return null;

            return requests;
        }

        public string RecieveRequest(int Id, int status)
        {
            DataContext _context = new DataContext();

            var request =  _context.AddressRequest.FirstOrDefault(x => x.Id == Id);
                if(request == null)
                    return null;

            if(status == 0){

                var validate =  _context.Childrens.FirstOrDefault(x => x.Id == request.ChildrenId);
                    if(validate == null)
                        return null;
                        
                validate.StartLat = request.StartLat;
                validate.StartLong = request.StartLong;
                validate.StartAddress = request.StartAddress;
                validate.EndLat = request.EndLat;
                validate.EndLong = request.EndLong;
                validate.EndAddress = request.EndAddress;

                _context.Childrens.Update(validate);
                _context.AddressRequest.Remove(request);
                _context.SaveChanges();

                return "Accepted";



            }else{
                 _context.AddressRequest.Remove(request);
                 _context.SaveChanges();

                 return "Removed";


            }
        }
    }
    
}

