﻿using HafelatyEntites;
using HafelatyServices.Dtos;
using HafelatyServices.Dtos.ToReturnDto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HafelatyServices
{
    public class AppServices
    {
        #region Define as Singleton
        private static AppServices _Instance;

        public static AppServices Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new AppServices();
                }

                return (_Instance);
            }
        }

        private AppServices()
        {
        }
        #endregion



        public Parents AddSecondParent(int primaryId, int secondaryId)
        {

            DataContext _context = new DataContext();


            var parentsRecord =  _context.Parents.FirstOrDefault(x => x.PrimaryParent == primaryId);

            if (parentsRecord == null)
                return null;



            parentsRecord.SSecondaryParet = secondaryId;


            _context.Parents.Update(parentsRecord);
             _context.SaveChanges();
            return parentsRecord;



        }

        public Parents AddFirstParent(int primaryId, int secondaryId)
        {
            DataContext _context = new DataContext();


            var parentsRecord =  _context.Parents.FirstOrDefault(x => x.PrimaryParent == primaryId);

            if (parentsRecord == null)
            {
                var recordToCreate = new Parents
                {
                    PrimaryParent = primaryId,
                    fSecondaryParent = secondaryId
                };

                 _context.Parents.Add(recordToCreate);
                _context.SaveChanges();

                return recordToCreate;
            }
            else
            {


                parentsRecord.fSecondaryParent = secondaryId;


                _context.Parents.Update(parentsRecord);
                _context.SaveChanges();

                return parentsRecord;
            }







        }



        public bool SecondaryFull(int pirmaryId)
        {

            DataContext _context = new DataContext();

            if ( _context.Parents.Any(x => x.PrimaryParent == pirmaryId && x.SSecondaryParet != null && x.fSecondaryParent != null))
            {
                return true;
            }

            return false;



        }

        public bool IsSecondarySecondExists(int pirmaryId)
        {

            DataContext _context = new DataContext();

            if ( _context.Parents.Any(x => x.PrimaryParent == pirmaryId && x.SSecondaryParet != null))
            {
                return true;
            }

            return false;



        }

        public bool IsSecondaryFirstExists(int pirmaryId)
        {

            DataContext _context = new DataContext();

            if ( _context.Parents.Any(x => x.PrimaryParent == pirmaryId && x.fSecondaryParent != null))
            {
                return true;
            }

            return false;



        }

        public Parents Delete(ParentToDeleteDto parentToDelete)
        {
            DataContext _context = new DataContext();

            var parentsRecord =  _context.Parents.FirstOrDefault(x => x.PrimaryParent == parentToDelete.PrimaryId);
            if (parentsRecord == null)
                return null;

            if (parentsRecord.fSecondaryParent == parentToDelete.SecondaryId)
            {
                parentsRecord.fSecondaryParent = null;

                var userTodelete =  _context.Users.FirstOrDefault(x => x.Id == parentToDelete.SecondaryId);
                userTodelete.IsDeleted = 1;

                _context.Parents.Update(parentsRecord);
                _context.Users.Update(userTodelete);

                _context.SaveChanges();

                return parentsRecord;

            }
            else if (parentsRecord.SSecondaryParet == parentToDelete.SecondaryId)
            {
                parentsRecord.SSecondaryParet = null;

                var userTodelete =  _context.Users.FirstOrDefault(x => x.Id == parentToDelete.SecondaryId);
                userTodelete.IsDeleted = 1;

                _context.Parents.Update(parentsRecord);
                _context.Users.Update(userTodelete);
                _context.SaveChanges();

                return parentsRecord;
            }
            else
            {

                return null;

            }

        }

        public bool VoucherIsValid(VoucherDto voucherDto)
        {

            DataContext _context = new DataContext();

            if ( _context.Verification.Any(x => x.CodeHash == voucherDto.Code && x.Used == 0))
                return true;

            return false;


        }

        public Childrens AddChild(ChildToAddDto childToAddDto, int parentRowId)
        {

            DataContext _context = new DataContext();

            var childToCreate = new Childrens
            {
                Firstname = childToAddDto.Firstname,
                Middlename = childToAddDto.Middlename,
                Lastname = childToAddDto.Lastname,
                Age = childToAddDto.Age,
                ParentsId = parentRowId,
                SchoolId = childToAddDto.SchoolId,
                StartLat = childToAddDto.StartLat,
                StartLong = childToAddDto.StartLong,
                StartAddress = childToAddDto.StartAddress,
                EndLat = childToAddDto.EndLat,
                EndLong = childToAddDto.EndLong,
                EndAddress = childToAddDto.EndAddress

            };

            var childToAdd =  _context.Childrens.Add(childToCreate);
             _context.SaveChanges();

            return childToCreate;


            // var childToAdd =  _context.Childrens.Add()
        }

        public Parents GetParentsRecord(int parentId)
        {

            DataContext _context = new DataContext();


            var parentsRecord =  _context.Parents.FirstOrDefault(x => x.PrimaryParent == parentId || x.SSecondaryParet == parentId || x.fSecondaryParent == parentId);
            if (parentsRecord == null)
                return null;


            return parentsRecord;
        }

        public Verification UseVoucher(VoucherDto voucherDto)
        {
            DataContext _context = new DataContext();

            var getVoucher =  _context.Verification.FirstOrDefault(x => x.CodeHash == voucherDto.Code);
            if (getVoucher == null)
                return null;

            getVoucher.Used = 1;

            _context.Verification.Update(getVoucher);
             _context.SaveChanges();
            return getVoucher;
        }

        public List<Childrens> GetChildsByParent(int primaryId)
        {

            DataContext _context = new DataContext();

            var Childrens =  _context.Childrens.Include(c => c.Passengers).Where(x => x.ParentsId == primaryId && x.IsDeleted == 0).ToList();
            // var Childrens =  _context.Childrens.Include(x => x.Passengers).ThenInclude(Passengers => Passengers.Childrens).ToList(x => x.ParentsId == primaryId && x.IsDeleted == 0);
            // var query =  _context.Childrens
            //       .Where(g => g.ParentsId == primaryId && g.IsDeleted == 0)
            //       .Select(Passengers=> new
            //       {
            //          Children = Passengers,
            //          address = cntct.Passengers.Where(p => p.Chi == 1),
            //          city    = cntct.Address.Where(p => p.AddressTypeID == 1)
            //                                 .Select(h=>h.City),
            // }.ToList();


            // foreach (var elem in Childrens){

            //     var image =  _context.Photos.FirstOrDefault(x => x.ImageFor == elem.Id && x.Type == "Children");

            //     images.Add(image);

            // }

            if (!Childrens.Any())
                return null;

            return Childrens;
        }

        public Childrens DeleteChild(ChildToDeleteDto childToDeleteDto)
        {

            DataContext _context = new DataContext();

            var child =  _context.Childrens.FirstOrDefault(x => x.Id == childToDeleteDto.Id);
            if (child == null)
                return null;

            child.IsDeleted = 1;

            var passenger =  _context.Passengers.FirstOrDefault(x => x.ChildrenId == childToDeleteDto.Id);
            if (passenger != null)
            {
                _context.Passengers.Remove(passenger);
            }

            var photo =  _context.Photos.FirstOrDefault(x => x.ImageFor == childToDeleteDto.Id && x.Type == "Children");
            if (photo != null)
            {
                _context.Photos.Remove(photo);

            };

            var childCase =  _context.ChildrensCases.FirstOrDefault(x => x.ChildrenId == childToDeleteDto.Id);
            if (childCase != null)
            {
                _context.ChildrensCases.Remove(childCase);

            }

            var roundPassenger =  _context.RoundPassengers.FirstOrDefault(x => x.ChildrenId == childToDeleteDto.Id);
            if (roundPassenger != null)
            {
                var busRound =  _context.BusRounds.FirstOrDefault(x => x.Id == roundPassenger.BusRoundId);
                busRound.CurrentAmount = busRound.CurrentAmount - 1;
                _context.BusRounds.Update(busRound);
                _context.RoundPassengers.Remove(roundPassenger);




            }

            _context.Childrens.Update(child);
             _context.SaveChanges();
            return child;
        }

        public Childrens EditChild(ChildToUpdateDto childToUpdateDto)
        {
            DataContext _context = new DataContext();

            var child =  _context.Childrens.FirstOrDefault(x => x.Id == childToUpdateDto.Id);
            if (child == null)
                return null;

            child.Firstname = childToUpdateDto.Firstname;
            child.Middlename = childToUpdateDto.Middlename;
            child.Lastname = childToUpdateDto.Lastname;
            child.Age = childToUpdateDto.Age;
            child.SchoolId = childToUpdateDto.SchoolId;

            _context.Childrens.Update(child);
             _context.SaveChanges();
            return child;
        }

        public bool GetChildByName(string firstName, string middlename, string lastName)
        {

            DataContext _context = new DataContext();

            var child =  _context.Childrens.FirstOrDefault(x => x.Firstname == firstName && x.Middlename == middlename && x.Lastname == lastName);
            if (child == null)
                return false;

            return true;

        }

        public string AddAddressToChild(updateAddressDto addressToAddDto)
        {
            DataContext _context = new DataContext();

            var child =  _context.Childrens.FirstOrDefault(x => x.Id == addressToAddDto.Id);
            if (child == null)
                return null;

            if (child.StartLat == null)
            {

                child.StartLat = addressToAddDto.StartLat;
                child.StartLong = addressToAddDto.StartLong;
                child.StartAddress = addressToAddDto.StartAddress;
                child.EndLat = addressToAddDto.EndLat;
                child.EndLong = addressToAddDto.EndLong;
                child.EndAddress = addressToAddDto.EndAddress;


                _context.Update(child);
                _context.SaveChanges();

            }
            else
            {

                var ChildToAddDto = new AddressRequest
                {
                    ChildrenId = addressToAddDto.Id,
                    StartLat = addressToAddDto.StartLat,
                    StartLong = addressToAddDto.StartLong,
                    StartAddress = addressToAddDto.StartAddress,
                    EndLat = addressToAddDto.EndLat,
                    EndLong = addressToAddDto.EndLong,
                    EndAddress = addressToAddDto.EndAddress

                };

                var childToAdd =  _context.AddressRequest.Add(ChildToAddDto);
                _context.SaveChanges();



            }


            return "Done";
        }

        public Childrens AddEndAddressToChild(EndAdressToAddDto addressToAddDto)
        {
            DataContext _context = new DataContext();

            var child =  _context.Childrens.FirstOrDefault(x => x.Id == addressToAddDto.Id);
            if (child == null)
                return null;

            child.EndLat = addressToAddDto.EndLat;
            child.EndLong = addressToAddDto.EndLong;
            child.EndAddress = addressToAddDto.EndAddress;

            _context.Update(child);
             _context.SaveChanges();
            return child;
        }

        public Childrens getChildAddress(int Id)
        {
            DataContext _context = new DataContext();


            var child =  _context.Childrens.FirstOrDefault(x => x.Id == Id);
            if (child == null)
                return null;

            return child;
        }

        public Parents addParentRecord(int Id)
        {
            DataContext _context = new DataContext();

            var recordToCreate = new Parents
            {
                PrimaryParent = Id
            };

             _context.Parents.Add(recordToCreate);
             _context.SaveChanges();
            return recordToCreate;
        }

        public List<User> getAdmins(int type)
        {
            DataContext _context = new DataContext();

            var users = new List<User>();
            if (type == 1)
            {
                users =  _context.Users.Where(x => x.UserTypeId == 1).ToList();
                if (!users.Any())
                    return null;


            }
            else if (type == 2)
            {
                users =  _context.Users.Where(x => x.UserTypeId == 1).ToList();
                if (!users.Any())
                    return null;

            }

            return users;
        }

        public AdminsToReturnDto GetAdminInfo(int Id)
        {
            DataContext _context = new DataContext();

            var adminInfo =  _context.UserInfo.FirstOrDefault(x => x.UserId == Id);
            if (adminInfo == null)
                return null;

            var returnedInfo = new AdminsToReturnDto();

            returnedInfo.Id = adminInfo.UserId;
            returnedInfo.Firstname = adminInfo.Firstname;
            returnedInfo.Lastname = adminInfo.Lastname;


            return returnedInfo;
        }

        public ChildrensCases TrackChildren(int childrenId, int type)
        {
            DataContext _context = new DataContext();

            var childrenRecord =  _context.ChildrensCases.FirstOrDefault(x => x.ChildrenId == childrenId);
            var childrenCase = new ChildrensCases();

            if (childrenRecord == null)
            {
                childrenCase.ChildrenId = childrenId;
                childrenCase.CaseId = 1;

                 _context.ChildrensCases.Add(childrenCase);


            }
            else
            {

                childrenCase.ChildrenId = childrenId;
                childrenCase.CaseId = type;

                childrenRecord.CaseId = type;
                _context.ChildrensCases.Update(childrenCase);


            }

             _context.SaveChanges();            
             return childrenCase;

        }

        public bool UseVoucherForChildren(int childrenId, int voucherCode)
        {
            DataContext _context = new DataContext();

            var voucher =  _context.Verification.FirstOrDefault(x => x.CodeHash == voucherCode && x.Used == 0);
            if (voucher == null)
                return false;

            voucher.Used = 1;
            _context.Verification.Update(voucher);


            var childrenRecord =  _context.ChildrensVouchers.FirstOrDefault(x => x.ChildrenId == childrenId);
            if (childrenRecord == null)
            {
                ChildrensVouchers record = new ChildrensVouchers();
                record.ChildrenId = childrenId;
                record.VoucherCode = voucherCode;
                record.EntryDate = DateTime.Now;
                record.ExpireDate = DateTime.Now.AddMonths(1);

                 _context.ChildrensVouchers.Add(record);

            }
            else
            {

                childrenRecord.VoucherCode = voucherCode;
                childrenRecord.EntryDate = DateTime.Now;
                childrenRecord.ExpireDate = DateTime.Now.AddMonths(1);

                _context.ChildrensVouchers.Update(childrenRecord);


            }
             _context.SaveChanges();
            return true;
        }

        public bool ValidateVoucher(int voucherCode)
        {
            DataContext _context = new DataContext();

            var voucher =  _context.Verification.FirstOrDefault(x => x.CodeHash == voucherCode && x.Used == 1);
            if (voucher != null)
                return false;

            return true;
        }

        public ChildrensVouchers GetChildrenVouchersRecord(int childrenId)
        {
            DataContext _context = new DataContext();

            var childrenVoucher =  _context.ChildrensVouchers.FirstOrDefault(x => x.ChildrenId == childrenId);
            if (childrenVoucher == null)
                return null;

            return childrenVoucher;
        }


    }
}
