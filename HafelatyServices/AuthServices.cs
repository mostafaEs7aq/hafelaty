﻿using CorePush.Google;
using HafelatyEntites;
using HafelatyServices.Dtos;
using HafelatyServices.Dtos.ToReturnDto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HafelatyServices
{
    public class AuthServices
    {
        #region Define as Singleton
        private static AuthServices _Instance;

        public static AuthServices Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new AuthServices();
                }

                return (_Instance);
            }
        }

        private AuthServices()
        {
        }
        #endregion

        public User Login(string phonenumber, string password, string deviceToken)
        {

            DataContext _context = new DataContext();



            var user =  _context.Users.FirstOrDefault(x => x.Phonenumber == phonenumber);

            if (user == null || user.IsDeleted == 1)
                return null;



            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return null;

            if (deviceToken != null)
            {

                user.DeviceToken = deviceToken;
                _context.Update(user);
                 _context.SaveChanges();

            }



            return user;


        }

        public UserInfo GetUserInfo(int? UserId)
        {
            DataContext _context = new DataContext();

            if (UserId == null)
                return null;

            var userInfo =  _context.UserInfo.FirstOrDefault(x => x.UserId == UserId);

            if (userInfo == null)
                return null;


            return userInfo;


        }

        public User GetUser(int? userId)
        {
            DataContext _context = new DataContext();

            var userToReturn = new User { };
            var user =  _context.Users.FirstOrDefault(x => x.Id == userId);

            if (user == null)
                return null;


            userToReturn.Id = user.Id;
            userToReturn.Username = user.Username;
            //  userToReturn.Email = user.Email;
            userToReturn.UserTypeId = user.UserTypeId;
            userToReturn.Phonenumber = user.Phonenumber;



            return userToReturn;


        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {

            DataContext _context = new DataContext();

            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));

                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != passwordHash[i]) return false;
                }
            }
            return true;


        }

        public User Register(User user, string password)
        {

            DataContext _context = new DataContext();



            byte[] passwordHash, passwordSalt;





            CreatePasswordHash(password, out passwordHash, out passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;



            var createdUser =  _context.Users.Add(user);
             _context.SaveChanges();

            return user;




        }

        public UserInfo AddInfo(UserInfo info)
        {

            DataContext _context = new DataContext();


            string firstname = info.Firstname;

            string middlename = info.Middlename;

            string lastname = info.Lastname;

            int age = info.Age;

            int userId = info.UserId;

            int gender = info.GenderId;

            info.Firstname = firstname;
            info.Middlename = middlename;
            info.Lastname = lastname;
            info.Age = age;
            info.GenderId = gender;
            // info.Phonenumber = phonenumber;
            // info.UserId = userId;

             _context.UserInfo.Add(info);
             _context.SaveChanges();

            return info;


        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }

        }

        public bool UserExists(string phonenumber)
        {

            DataContext _context = new DataContext();

            //  if ( _context.Users.Any(x => x.Phonenumber == phonenumber))
            //  {
            //      return true;
            //  }

            //  return false;

            var validate =  _context.Users.FirstOrDefault(x => x.Phonenumber == phonenumber);
            if (validate == null)
            {

                return false;

            }
            else if (validate.IsDeleted == 1)
            {

                return false;

            }
            else
            {

                return true;
            }




        }

        public UserInfo EditProfile(UserInfo editInfo)
        {


            DataContext _context = new DataContext();


            string firstname = editInfo.Firstname;
            string middlename = editInfo.Middlename;
            string lastname = editInfo.Lastname;
            int userId = editInfo.UserId;
            // string phonenumber = editInfo.Phonenumber;

            var userRecord =  _context.UserInfo.FirstOrDefault(x => x.UserId == userId);

            if (userRecord == null)
                return null;


            userRecord.Firstname = firstname;
            userRecord.Middlename = middlename;
            userRecord.Lastname = lastname;
            // userRecord.Phonenumber = phonenumber;



            _context.UserInfo.Update(userRecord);
             _context.SaveChanges();

            return editInfo;


        }

        public User ChangePassword(PasswordsDto passwords)
        {

            DataContext _context = new DataContext();


            int UserId = passwords.UserId;
            byte[] NewPasswordHash, NewPasswordSalt;

            var userRecord =  _context.Users.FirstOrDefault(x => x.Id == UserId);

            if (userRecord == null)
                return null;

            if (!VerifyPasswordHash(passwords.OldPassword, userRecord.PasswordHash, userRecord.PasswordSalt))
                return null;

            CreatePasswordHash(passwords.NewPassword, out NewPasswordHash, out NewPasswordSalt);

            userRecord.PasswordHash = NewPasswordHash;
            userRecord.PasswordSalt = NewPasswordSalt;



            _context.Users.Update(userRecord);
             _context.SaveChanges();

            return userRecord;




        }


        public Bus HaveBus(int UserId)
        {
            DataContext _context = new DataContext();

            var Driver =  _context.Bus.FirstOrDefault(x => x.DriverId == UserId);

            return Driver;
        }

        public RoundPassengers updateAttendanceStatus(AttendanceDto attendanceDto)
        {
            DataContext _context = new DataContext();

            var passenger =  _context.RoundPassengers.FirstOrDefault(x => x.ChildrenId == attendanceDto.ChildrenId);
            if (passenger == null)
                return null;

            passenger.Attendance = attendanceDto.Status;

            _context.RoundPassengers.Update(passenger);
             _context.SaveChanges();



            return passenger;


        }

        public string SendNotifications(int parentsRecordId, string type, string childName, int? busId)
        {
            DataContext _context = new DataContext();


            string childAbscentFromHomePickUp = "absent/busPickUpAtHome";
            string childAbscentFromSchoolDropOf = "absent/busDropAtSchool";
            string childAbscentFromSchoolPickUp = "absent/busPickUpAtSchool";
            string childAbscentAtHomeDropOf = "absent/busDropAtHome";
            string childPresenceFromHomePickUp = "with/busPickUpAtHome";
            string childPresenceFromSchoolDropOf = "with/busDropAtSchool";
            string childPresenceFromSchoolPickUp = "with/busPickUpAtSchool";
            string childPresenceAtHomeDropOf = "with/busDropAtHome";
            string arrived = "arrived";
            string body;
            string title = "حافلة";
            string apiKey = "AAAA1XPxo-k:APA91bE_wb96OCFJNVMrjJAUm8MXtW6Yr5M2ipAdqwCdWGb6hbw_joU1XXV9TopYOy55XlSSntfH7WaRThL5hD_2RJMzSUxsaE-8mVw6u7j2htnLq9_5N_XHydqqrpudAvo7BWACa4FI";
            string apiUser = "Hafelah Company";
            int? sentBusId = null;


            if (type == childAbscentFromHomePickUp)
            {
                body = childName + "لم يركب في جولة الصباح من بيته";
                sentBusId = busId;

            }
            else if (type == childAbscentFromSchoolDropOf)
            {

                body = childName + "لم ينزل من جولة الصباح الى مدرسته";
                sentBusId = busId;

            }
            else if (type == childAbscentFromSchoolPickUp)
            {

                body = childName + "لم يركب في جولة المساء من مدرسته";
                sentBusId = busId;


            }
            else if (type == childAbscentAtHomeDropOf)
            {

                body = childName + "لم ينزل من جولة المساء الى بيته";
                sentBusId = busId;


            }
            else if (type == childPresenceFromHomePickUp)
            {

                body = childName + "ركب في جولة الصباح من بيته";
                sentBusId = busId;

            }
            else if (type == childPresenceFromSchoolDropOf)
            {

                body = childName + "نزل من جولة الصباح الى مدرسته";
                sentBusId = busId;

            }
            else if (type == childPresenceFromSchoolPickUp)
            {

                body = childName + "ركب في جولة المساء من مدرسته";
                sentBusId = busId;


            }
            else if (type == childPresenceAtHomeDropOf)
            {

                body = childName + "نزل من جولة المساء الى بيته";
                sentBusId = busId;

            }
            else if (type == arrived)
            {

                body = "اقتربت الحافلة من المنزل";
                sentBusId = busId;
            }
            else
            {

                body = "حدث خلل";

            }

            var parents =  _context.Parents.FirstOrDefault(x => x.Id == parentsRecordId);

            var primary =  _context.Users.FirstOrDefault(x => x.Id == parents.PrimaryParent);
            using var fcm = new FcmSender(apiKey, apiUser);
             fcm.SendAsync(primary.DeviceToken,
                new
                {
                    data = new
                    {
                        title = title,
                        body = body,
                        userId = $"{primary.Id}",
                        busId = sentBusId
                    },
                });

            if (parents.SSecondaryParet != null)
            {
                var secondSecondary =  _context.Users.FirstOrDefault(x => x.Id == parents.SSecondaryParet);
                if (secondSecondary.DeviceToken != null)
                {
                    using var fcmClient = new FcmSender(apiKey, apiUser);
                     fcmClient.SendAsync(secondSecondary.DeviceToken,
                        new
                        {
                            data = new
                            {
                                title = title,
                                body = body,
                                userId = $"{secondSecondary.Id}",
                                busId = sentBusId


                            },
                        });
                }

            }

            if (parents.fSecondaryParent != null)
            {
                var firstSecondary =  _context.Users.FirstOrDefault(x => x.Id == parents.fSecondaryParent);
                if (firstSecondary.DeviceToken != null)
                {
                    using var fcmClient = new FcmSender(apiKey, apiUser);
                     fcmClient.SendAsync(firstSecondary.DeviceToken,
                        new
                        {
                            data = new
                            {
                                title = title,
                                body = body,
                                userId = $"{firstSecondary.Id}",
                                busId = sentBusId


                            },
                        });
                }
            }


            return "Done";

        }

        public User Logout(int userId)
        {
            DataContext _context = new DataContext();

            var user =  _context.Users.FirstOrDefault(x => x.Id == userId);
            if (user == null)
                return null;

            user.IsLogginedIn = 0;
            _context.Users.Update(user);

            var bus =  _context.Bus.FirstOrDefault(x => x.DriverId == userId);
            if (bus != null && bus.Status == "On")
            {


                bus.Status = "Off";
                _context.Bus.Update(bus);

                var driverBehaviour = new DriverBehaviour();
                driverBehaviour.DriverId = userId;
                driverBehaviour.ActionTime = DateTime.Now;

                 _context.DriverBehaviours.Add(driverBehaviour);

            }




             _context.SaveChanges();



            return user;
        }

        public User ChangePhonenumber(int userId, string phonenumber)
        {
            DataContext _context = new DataContext();

            var user =  _context.Users.FirstOrDefault(x => x.Id == userId && x.IsDeleted == 0);
            if (user == null)
                return null;

            user.Phonenumber = phonenumber;
            _context.Users.Update(user);
             _context.SaveChanges();

            return user;


        }

        public User ForgetPassword(string phonenumber, string password)
        {
            DataContext _context = new DataContext();

            var user =  _context.Users.FirstOrDefault(x => x.Phonenumber == phonenumber);
            if (user == null)
                return null;

            byte[] NewPasswordHash, NewPasswordSalt;

            CreatePasswordHash(password, out NewPasswordHash, out NewPasswordSalt);

            user.PasswordHash = NewPasswordHash;
            user.PasswordSalt = NewPasswordSalt;

            _context.Users.Update(user);
             _context.SaveChanges();


            return user;



        }
    }
}

