﻿using HafelatyEntites;
using HafelatyServices.Dtos;
using HafelatyServices.Dtos.ToReturnDto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HafelatyServices
{
    public class DriverServices
    {
        #region Define as Singleton
        private static DriverServices _Instance;

        public static DriverServices Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new DriverServices();
                }

                return (_Instance);
            }
        }

        private DriverServices()
        {
        }
        #endregion


        
        public List<Childrens> ChildrensByBus(int BusId, string status)
        {
            DataContext _context = new DataContext();


            var validateBus =  _context.Passengers.FirstOrDefault(x => x.BusId == BusId);
            if (validateBus == null)
                return null;


            // var childrens =  _context.Childrens.Where(x => x.Passengers.BusId == BusId && x.EndAddress != null).ToList();
            var childrens =  _context.Childrens.Where(c => c.Passengers.BusId == BusId).ToList();
            if (!childrens.Any())
                return null;

            var bus =  _context.Bus.FirstOrDefault(x => x.Id == BusId);
            if (status == "start1")
            {
                bus.Status = "On";
                bus.Trip = 1;

            }
            else if (status == "start2")
            {

                bus.Status = "On";
                bus.Trip = 2;
            }
            else if (status == "end1")
            {

                bus.Status = "Off";
                bus.Trip = 1;

                var Passengers =  _context.Passengers.Where(x => x.BusId == BusId).ToList();
                foreach (var elem in Passengers)
                {
                    elem.Attendance = 0;
                    _context.Update(elem);
                     _context.SaveChanges();
                };


            }
            else
            {

                bus.Status = "Off";
                bus.Trip = 2;

                var Passengers =  _context.Passengers.Where(x => x.BusId == BusId).ToList();
                foreach (var elem in Passengers)
                {
                    elem.Attendance = 0;
                    _context.Update(elem);
                     _context.SaveChanges();
                };

            }



            _context.Bus.Update(bus);
             _context.SaveChanges();



            return childrens;
        }

        public List<object> GetChildrensBySchool(int busId, int schoolId)
        {
            DataContext _context = new DataContext();

            dynamic obj;
            var array = new List<object>();
            string Url;

            var childrens =  _context.Childrens.Where(x => x.SchoolId == schoolId).ToList();
                if(!childrens.Any())
                    return null;

            foreach(var child in childrens)
            {
                var passenger =  _context.Passengers.FirstOrDefault(x => x.ChildrenId == child.Id && x.BusId == busId);
                var parents =  _context.Parents.FirstOrDefault(x => x.Id == child.ParentsId);
                var parentRecord =  _context.Users.FirstOrDefault(x => x.Id == parents.PrimaryParent);
                var image =  PhotoServices.Instance.getUserImage("Children", child.Id);
                if(image == null)
                {
                    Url = null;
                }else{

                    Url = image.Url;

                }


                obj = new {
                    Id = child.Id,
                    Name= child.Firstname + " " + child.Lastname,
                    Lat = child.EndLat,
                    Lng = child.EndLong,
                    Area = child.EndAddress,
                    Type = "Children",
                    Phonenumber = parentRecord.Phonenumber,
                    Url = Url
                };

                array.Add(obj);


            }
            

            return array;

            
        }

        public List<Emergency> GetEmergencyCases(string type)
        {
            DataContext _context = new DataContext();

            var emergency =  _context.Emergencies.Where(x => x.Type == type).ToList();
            if(!emergency.Any())    
                return null;

            return emergency;

        }

        public IEnumerable<object> GetSchoolsByBus(int busId)
        {

            DataContext _context = new DataContext();


            var schools = new List<RetunedSchools>();
            var passengers =  _context.Passengers.Where(x => x.BusId == busId).ToList();
                if(!passengers.Any())
                    return null;
                
            foreach(var passenger in passengers)
            {
                var children =  _context.Childrens.FirstOrDefault(x => x.Id == passenger.ChildrenId);
                if(children == null)
                    return null;

                var school =  _context.Schools.FirstOrDefault(x => x.Id == children.SchoolId);
                    if(school == null)
                        return null;
                        
                 var obj = new RetunedSchools();

                obj.Id = school.Id;
                obj.Lat = school.Lat;
                obj.Lng = school.Long;
                obj.Area = school.Address;
                obj.Type = "School";
                obj.Name = school.SchoolName;

                schools.Add(obj);

            }

            var uiqueSchools = schools.GroupBy(p => p.Id).Select(grp => grp.First()).ToArray();


            return uiqueSchools;

        }

        public Childrens RecieveChild(RecieveChildDto recieveChildDto)
        {
            DataContext _context = new DataContext();

            var child =  _context.Childrens.FirstOrDefault(x => x.Id == recieveChildDto.ChildrenId);
            if (child == null)
                return null;


            var Passengers =  _context.Passengers.FirstOrDefault(x => x.ChildrenId == child.Id);
            if (Passengers == null)
                return null;



            Passengers.Attendance = recieveChildDto.Status;

            _context.Update(Passengers);
             _context.SaveChanges();

            return child;
        }

        public List<Schools> SchoolsByBus(int BusId)
        {
            DataContext _context = new DataContext();

            var schools = new List<Schools>();
            var childrens =  _context.Passengers.Where(x => x.BusId == BusId).ToList();
            if (!childrens.Any())
                return null;

            foreach (var elem in childrens)
            {
                var child =  _context.Childrens.FirstOrDefault(x => x.Id == elem.ChildrenId && x.IsDeleted == 0);
                if (child == null)
                {
                    schools.Add(null);
                }
                var school =  _context.Schools.FirstOrDefault(x => x.Id == child.SchoolId && x.IsDeleted == 0);
                if (school == null)
                {

                    schools.Add(null);

                }
                else
                {

                    schools.Add(school);

                }


            }

            return schools;
        }

        public EmergencyRequest SendEmergency(EmergencyDto emergencyDto)
        {
            DataContext _context = new DataContext();


            var emergency = new EmergencyRequest();
            emergency.DriverId = emergencyDto.DriverId;
            emergency.EmergencyId = emergencyDto.EmergencyId;
             _context.EmergencyRequests.Add(emergency);
             _context.SaveChanges();

            return emergency;
        }
    }
    
}

