namespace HafelatyServices.Dtos
{
    public class AddressToAddDto
    {

        public int Id { get; set; }

        public float StartLat { get; set; }

        public float StartLong { get; set; }

        public string StartAddress { get; set; }


    }
}