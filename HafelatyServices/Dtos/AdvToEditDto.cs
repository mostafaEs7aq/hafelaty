namespace HafelatyServices.Dtos
{
    public class AdvToEditDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Descreption { get; set; }

    }
}