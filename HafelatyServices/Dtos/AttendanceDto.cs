namespace HafelatyServices.Dtos
{
    public class AttendanceDto
    {

        public int ChildrenId { get; set; }

        public int ActionTypeId { get; set; }

        public int Status { get; set; }
    }
}