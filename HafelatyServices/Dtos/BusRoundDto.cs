namespace HafelatyServices.Dtos
{
    public class BusRoundDto
    {
        public int BusId { get; set; }

        public string TripName { get; set; }
        public int RoundTimesId { get; set; }
        public int RoundTypesId { get; set; }
    }
}