namespace HafelatyServices.Dtos
{
    public class BusRoundToEditDto
    {

        public int Id { get; set; }
        public int BusId { get; set; }

        public string TripName { get; set; }

        public int RoundTimesId { get; set; }
        public int RoundTypesId { get; set; }
    }
}