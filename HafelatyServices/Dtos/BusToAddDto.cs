namespace HafelatyServices.Dtos
{
    public class BusToAddDto
    {
        public int DriverId { get; set; }

        public int Capacity { get; set; }

        public string Area { get; set; }

        public string Model { get; set; }

        public string Plate { get; set; }

        public string Licenses { get; set; }

    }
}