namespace HafelatyServices.Dtos
{
    public class ChangePhonenumberDto
    {
        public int Id { get; set; }

        public string Phonenumber { get; set; }
    }
}