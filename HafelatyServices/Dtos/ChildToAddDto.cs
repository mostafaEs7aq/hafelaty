using System;

namespace HafelatyServices.Dtos
{
    public class ChildToAddDto
    {

        public string Firstname { get; set; }

        public string Middlename { get; set; }

        public string Lastname { get; set; }

        public string Age { get; set; }

        public int ParentId { get; set; }

        public int SchoolId { get; set; }

        public int Code { get; set; }

        public float StartLat { get; set; }

        public float StartLong { get; set; }

        public string StartAddress { get; set; }

        public float EndLat { get; set; }

        public float EndLong { get; set; }

        public string EndAddress { get; set;}
    }
}