namespace HafelatyServices.Dtos
{
    public class ChildToDeleteDto
    {
        public int Id { get; set; }
    }
}