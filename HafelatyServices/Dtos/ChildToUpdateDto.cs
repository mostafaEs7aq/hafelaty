using System;

namespace HafelatyServices.Dtos
{
    public class ChildToUpdateDto
    {
        public int Id { get; set; }

        public string Firstname { get; set; }

        public string Middlename { get; set; }

        public string Lastname { get; set; }


        public string Age { get; set; }

        public int SchoolId { get; set; }

    }
}