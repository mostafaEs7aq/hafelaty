namespace HafelatyServices.Dtos
{
    public class CommonIdDto
    {
        public int Id { get; set; }
    }
}