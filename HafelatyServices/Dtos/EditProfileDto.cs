using System.ComponentModel.DataAnnotations;

namespace HafelatyServices.Dtos
{
    public class EditProfileDto
    {
        [Required]
        public string Firstname { get; set; }

        public string Middlename { get; set; }

        [Required]
        public string Lastname { get; set; }

        public int UserId { get; set; }
    }
}