namespace HafelatyServices.Dtos
{
    public class EmergencyDto
    {
        public int DriverId { get; set; }

        public int EmergencyId { get; set; }
    }
}