﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HafelatyServices.Dtos
{
    public class EndAdressToAddDto
    {
        public int Id { get; set; }

        public float EndLat { get; set; }

        public float EndLong { get; set; }

        public string EndAddress { get; set; }
    }
}
