namespace HafelatyServices.Dtos
{
    public class ForgetPasswordDto
    {
        public string Phonenumber { get; set; }
        public string Password { get; set; }
    }
}