namespace HafelatyServices.Dtos
{
    public class ParentToDeleteDto
    {
        public int PrimaryId { get; set; }
        public int SecondaryId { get; set; }
    }
}