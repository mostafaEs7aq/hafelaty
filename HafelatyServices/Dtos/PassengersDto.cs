namespace HafelatyServices.Dtos
{
    public class PassengersDto
    {
        public int ChildrenId { get; set; }

        public int BusId { get; set; }
    }
}