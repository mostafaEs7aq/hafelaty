using System.ComponentModel.DataAnnotations;

namespace HafelatyServices.Dtos
{
    public class PasswordsDto
    {
        [Required]
        [StringLength(8 , MinimumLength = 4, ErrorMessage = "You Must Specify a password between 4 and 8")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(8 , MinimumLength = 4, ErrorMessage = "You Must Specify a password between 4 and 8")]
        public string NewPassword { get; set; }

        [Required]
        public int UserId { get; set; }


    }
}