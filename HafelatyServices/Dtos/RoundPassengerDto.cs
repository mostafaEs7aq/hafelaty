namespace HafelatyServices.Dtos
{
    public class RoundPassengerDto
    {
        public int ChildrenId { get; set; }

        public int BusRoundId { get; set; }
    }
}