namespace HafelatyServices.Dtos.ToReturnDto
{
    public class AdminsToReturnDto
    {
        public int Id { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }
    }
}