namespace HafelatyServices.Dtos.ToReturnDto
{
    public class BusRoundsProprties
    {
        public int Id { get; set; }

        public string TripName { get; set; }

        public int BusId { get; set; }

        public string BusName { get; set; }

        public int RoundTimesId { get; set; }

        public string RoundTime { get; set; }

        public int RoundTypesId { get; set; }

        public string RoundType { get; set; }
    }
}