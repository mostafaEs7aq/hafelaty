namespace HafelatyServices.Dtos.ToReturnDto
{
    public class BusRoundsToReturnDto
    {
        public int BusId { get; set; }
        public int RoundTimesId { get; set; }
        public int RoundTypesId { get; set; }
    }
}