namespace HafelatyServices.Dtos.ToReturnDto
{
    public class BusesAmountsToReturn
    {
        public int Id { get; set; }

        public string Model { get; set; }

        public string Area { get; set; }

        public string Plate { get; set; }

        public string Licenses { get; set; }

        public int Space { get; set; }
    }
}