namespace HafelatyServices.Dtos.ToReturnDto
{
    public class BusesToReturn
    {
        public int Id { get; set; }

        public int DriverId { get; set; }

        public int Capacity { get; set; }

        public int CurrentAmount { get; set; }

        public string Area { get; set; }

        public string Model { get; set; }

        public string Plate { get; set; }

        public string Licenses { get; set; }

        public int Space { get; set; }

    }
}