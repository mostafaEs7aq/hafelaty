namespace HafelatyServices.Dtos.ToReturnDto
{
    public class ChildToReturnDto
    {
        public int Id { get; set; }

        public string Firstname { get; set; }

        public string Middlename { get; set; }

        public string Lastname { get; set; }

        public string Age { get; set; }

        public float StartLat { get; set; }

        public float StartLong { get; set; }

        public string StartAddress { get; set; }

        public float EndLat { get; set; }
        
        public float EndLong { get; set; }

        public string EndAddress { get; set; }

        // public string Url { get; set; }

        public int SchoolId { get; set; }    

        public int Trip { get; set; }    

        // public Passengers Passengers { get; set; }
    }
}