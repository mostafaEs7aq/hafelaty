namespace HafelatyServices.Dtos.ToReturnDto
{
    public class DriverLocationToReturnDto
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}