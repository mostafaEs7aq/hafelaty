namespace HafelatyServices.Dtos.ToReturnDto
{
    public class DriverToReturnDto
    {
        public int Id { get; set; }

        public string Username { get; set; }
        
        public string Phonenumber { get; set; }
    }
}