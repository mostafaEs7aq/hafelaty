namespace HafelatyServices.Dtos.ToReturnDto
{
    public class PassengersToReturnDto
    {
            public int Id { get; set; }
            public string Firstname { get; set; }
            
            public string MiddleName { get; set; }
            public string Lastname { get; set; }

            public string Age { get; set; }

            public float? Lat { get; set; }

            public float? Lng { get; set; }

            public string Address { get; set; }

            public int SchoolId { get; set; }

            public string Phonenumber { get; set; }

            public string Url { get; set; }
    }
}