namespace HafelatyServices.Dtos.ToReturnDto
{
    public class SchoolsToReturnDto
    {
        public int Id { get; set; }
        public string SchoolName { get; set; }

        public string Address { get; set; }

        public string Phonenumber { get; set; }

        public float Lat { get; set; }

        public float Long { get; set; }
    }
}