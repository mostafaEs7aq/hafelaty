
namespace HafelatyServices.Dtos.ToReturnDto
{
    public class StartTripToReturnDto
    {
        public int Id { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public float? StartLat { get; set; }

        public float? StartLong { get; set; }

        public string StartAddress { get; set; }

        public int Attendance { get; set; }


        // public int Attendance { get; set; }

        // public PassengersToReturnDto Passengers { get; set; }
        

        



        
    }
}