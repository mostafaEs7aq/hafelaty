namespace HafelatyServices.Dtos.ToReturnDto
{
    public class TimesToReturnDto
    {
        public int Id { get; set; }
        public string Times { get; set; }
    }
}