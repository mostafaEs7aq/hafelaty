namespace HafelatyServices.Dtos.ToReturnDto
{
    public class TypesToReturnDto
    {
        public int Id { get; set; }

        public string Type { get; set; }
    }
}