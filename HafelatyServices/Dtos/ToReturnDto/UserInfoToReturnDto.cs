
using HafelatyEntites;

namespace HafelatyServices.Dtos.ToReturnDto
{
    public class UserInfoToReturnDto
    {
        public int UserType { get; set; }

        public string Firstname { get; set; }

        public string Middlename { get; set; }

        public string Lastname { get; set; }

        public Gender Gender { get; set; }

        public int GenderId { get; set; }
    }
}