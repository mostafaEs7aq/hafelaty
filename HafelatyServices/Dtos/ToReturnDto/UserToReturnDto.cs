using System.Collections;
using System.Collections.Generic;

namespace HafelatyServices.Dtos.ToReturnDto
{
    public class UserToReturnDto
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public string PhotoUrl { get; set; }

        public string Phonenumber { get; set; }

        public ICollection<UserInfoToReturnDto> Info { get; set; }
        public ICollection<PhotoToReturnDto> Photo { get; set; }
    }
}