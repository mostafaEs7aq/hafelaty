namespace HafelatyServices.Dtos.ToReturnDto
{
    public class VouchersToReturn
    {
        public int Id { get; set; }
        public string CodeHash { get; set; }
    }
}