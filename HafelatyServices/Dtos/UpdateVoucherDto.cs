namespace HafelatyServices.Dtos
{
    public class UpdateVoucherDto
    {
        public int ChildrenId { get; set; }
        public int Code { get; set; }
    }
}