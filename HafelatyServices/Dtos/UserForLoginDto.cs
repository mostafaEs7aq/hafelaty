namespace HafelatyServices.Dtos
{
    public class UserForLoginDto
    {
        public string Phonenumber { get; set; }
        public string Password { get; set; }

        public string DeviceToken { get; set; }
    }
}