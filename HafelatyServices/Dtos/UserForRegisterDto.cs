using System.ComponentModel.DataAnnotations;

namespace HafelatyServices.Dtos
{
    public class UserForRegisterDto
    {
        [Required]
        public string Username { get; set; }
        
        [Required]
        [StringLength(8 , MinimumLength = 4, ErrorMessage = "You Must Specify a password between 4 and 8")]
        public string Password { get; set; }

        // [Required]
        // [EmailAddress]
        // public string Email { get; set; }

        [Required]
        public string Phonenumber { get; set; }

        public string Firstname { get; set; }

        public string Middlename { get; set; }

        public string Lastname { get; set; }

        public int GenderId { get; set; }
        

        public int Age { get; set; }

        public int UserTypeId { get; set; }

        public string DeviceToken { get; set; } 


    }
}