﻿using HafelatyEntites;
using HafelatyServices.Dtos;
using HafelatyServices.Dtos.ToReturnDto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HafelatyServices
{
    public class JointServices
    {
        #region Define as Singleton
        private static JointServices _Instance;

        public static JointServices Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new JointServices();
                }

                return (_Instance);
            }
        }

        private JointServices()
        {
        }
        #endregion


        
        public Schools GetSchool(int id)
        {
            DataContext _context = new DataContext();

            var school =  _context.Schools.FirstOrDefault(x => x.Id == id);

            return school;
        }

        public List<Schools> GetSchools()
        {
            DataContext _context = new DataContext();

            var Schools =  _context.Schools.Where(x => x.IsDeleted == 0).ToList();

            return Schools;
        }

    }
    
}

