﻿using HafelatyEntites;
using HafelatyServices.Dtos;
using HafelatyServices.Dtos.ToReturnDto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HafelatyServices
{
    public class PhotoServices
    {
        #region Define as Singleton
        private static PhotoServices _Instance;

        public static PhotoServices Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new PhotoServices();
                }

                return (_Instance);
            }
        }

        private PhotoServices()
        {
        }
        #endregion


          public Advertisment addAdvertisment(Advertisment advToAddDto)
        {
            DataContext _context = new DataContext();

            var advToAdd =  _context.Advertisments.Add(advToAddDto);
                 _context.SaveChanges();

                return advToAddDto;
        }

        public Offers addOffer(Offers offerToAddDto)
        {
            DataContext _context = new DataContext();

            var offerToAdd =  _context.HotOffers.Add(offerToAddDto);
                 _context.SaveChanges();

                return offerToAddDto;
        }

        public Advertisment DeleteAdv(CommonIdDto commonIdDto)
        {
            DataContext _context = new DataContext();

            var adv =  _context.Advertisments.FirstOrDefault(x => x.Id == commonIdDto.Id);
                if(adv == null)
                    return null;

            adv.IsDeleted = 1;

            _context.Advertisments.Update(adv);
             _context.SaveChanges();

            return adv;
        }

        public Offers DeleteOffer(CommonIdDto commonIdDto)
        {
            DataContext _context = new DataContext();

            var offer =  _context.HotOffers.FirstOrDefault(x => x.Id == commonIdDto.Id);
                if(offer == null)
                    return null;

            offer.IsDeleted = 1;

            _context.HotOffers.Update(offer);
             _context.SaveChanges();

            return offer;        
        }

        public Advertisment EditAdv(string Name, string Descreption,int Id, string Url)
        {
            DataContext _context = new DataContext();

            var adv =  _context.Advertisments.FirstOrDefault(x => x.Id == Id);
                if(adv == null)
                    return null;

            adv.Name = Name;
            adv.Descreption = Descreption;
            adv.Url = Url;

            _context.Advertisments.Update(adv);
             _context.SaveChanges();

            return adv;
        }

        public List<Advertisment> GetAds()
        {
            DataContext _context = new DataContext();

            var Ads =  _context.Advertisments.Where(x => x.IsDeleted == 0).ToList();

            return Ads;
        }

        public List<Offers> GetOffers()
        {
            DataContext _context = new DataContext();

            var Offers =  _context.HotOffers.Where(x => x.IsDeleted == 0).ToList();

            return Offers;        }

        public Photo getUserImage(string type, int? userId)
        {
            DataContext _context = new DataContext();

            var image =  _context.Photos.FirstOrDefault(x => x.ImageFor == userId && x.Type == type);
            if(image == null)
                return null;

            return image;
        }

        public bool isAdvNameUsed(string name)
        {
            DataContext _context = new DataContext();

            if ( _context.Advertisments.Any(x => x.Name == name))
                {
                    return true;
                }

                return false;
        }

        public bool isAvatarExisit(string type, int userId)
        {
            DataContext _context = new DataContext();

            if ( _context.Photos.Any(x => x.Type == type && x.ImageFor == userId))
                {
                    return true;
                }

                return false;
        }

        public Photo uploadAvatar(Photo photoToAdd, string action)
        {
            DataContext _context = new DataContext();

            if (action == "add"){

                var recordToAdd =  _context.Photos.Add(photoToAdd);
                 _context.SaveChanges();

                }else{ //update

                    var record =  _context.Photos.FirstOrDefault(x => x.ImageFor == photoToAdd.ImageFor && x.Type == photoToAdd.Type);
                        if(record == null)
                            return null;


                    record.Url = photoToAdd.Url;

                    _context.Photos.Update(record);
                     _context.SaveChanges();

                }


                return photoToAdd;
        }
    }
    
}

