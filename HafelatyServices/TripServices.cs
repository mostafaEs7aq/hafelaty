﻿using HafelatyEntites;
using HafelatyServices.Dtos;
using HafelatyServices.Dtos.ToReturnDto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HafelatyServices
{
    public class TripServices
    {
        #region Define as Singleton
        private static TripServices _Instance;

        public static TripServices Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new TripServices();
                }

                return (_Instance);
            }
        }

        private TripServices()
        {
        }
        #endregion


         public BusRound AddBusRound(BusRoundDto busRoundDto)
        {
            DataContext _context = new DataContext();

            var bus =  GetRoundBusRecord(busRoundDto.BusId);
            var busToAdd = new BusRound();

            busToAdd.BusId = busRoundDto.BusId;
            busToAdd.RoundTimesId = busRoundDto.RoundTimesId;
            busToAdd.RoundTypesId = busRoundDto.RoundTypesId;
            busToAdd.TripName = busRoundDto.TripName;
            busToAdd.Capacity = bus.Capacity;

             _context.BusRounds.Add(busToAdd);
             _context.SaveChanges();

            return busToAdd;

        }

        public bool DeleteBusRound(CommonIdDto commonIdDto)
        {
            DataContext _context = new DataContext();

            var busRound =  _context.BusRounds.FirstOrDefault(x => x.Id == commonIdDto.Id);
            if (busRound == null)
                return false;

            var roundPassengers =  _context.RoundPassengers.Where(x => x.BusRoundId == busRound.Id).ToList();
            if (roundPassengers.Any())
            {
                foreach (var passenger in roundPassengers)
                {
                    _context.RoundPassengers.Remove(passenger);
                }
            }


            busRound.IsDeleted = 1;
            _context.BusRounds.Update(busRound);
             _context.SaveChanges();

            return true;
        }

        public BusRound EditBusRound(BusRoundToEditDto busRoundDto)
        {
            DataContext _context = new DataContext();

            var busRound =  _context.BusRounds.FirstOrDefault(x => x.Id == busRoundDto.Id);
            if (busRound == null)
                return null;

            busRound.BusId = busRoundDto.BusId;
            busRound.RoundTypesId = busRoundDto.RoundTypesId;
            busRound.RoundTimesId = busRoundDto.RoundTimesId;
            busRound.TripName = busRoundDto.TripName;

            _context.BusRounds.Update(busRound);
             _context.SaveChanges();

            return busRound;

        }

        public List<object> GetBusRounds(int busId)
        {
            DataContext _context = new DataContext();

            var busRounds =  _context.BusRounds.Where(x => x.BusId == busId && x.IsDeleted == 0).ToList();
            if (busRounds == null)
                return null;

            var response = new List<Object>();

            foreach (var elem in busRounds)
            {
                RoundTypes type = GetRoundTypeRecord(elem.RoundTypesId);
                Bus bus = GetRoundBusRecord(busId);
                RoundTimes time = GetRoundTimeRecord(elem.RoundTimesId);
                var rounds = new BusRoundsProprties();

                rounds.Id = elem.Id;
                rounds.TripName = elem.TripName;
                rounds.BusId = elem.BusId;
                rounds.BusName = bus.Model;
                rounds.RoundTimesId = elem.RoundTimesId;
                rounds.RoundTime = time.Times;
                rounds.RoundTypesId = elem.RoundTypesId;
                rounds.RoundType = type.Type;


                response.Add(rounds);
            }

            return response;
        }

        public List<Object> GetAllBusesRounds()
        {
            DataContext _context = new DataContext();

            var allBusRounds =  _context.BusRounds.Where(x => x.IsDeleted == 0).ToList();
            if (!allBusRounds.Any())
                return null;

            var response = new List<Object>();

            foreach (var elem in allBusRounds)
            {
                RoundTypes type =  GetRoundTypeRecord(elem.RoundTypesId);
                Bus bus =  GetRoundBusRecord(elem.BusId);
                RoundTimes time =  GetRoundTimeRecord(elem.RoundTimesId);
                var rounds = new BusRoundsProprties();

                rounds.Id = elem.Id;
                rounds.TripName = elem.TripName;
                rounds.BusId = elem.BusId;
                rounds.BusName = bus.Model;
                rounds.RoundTimesId = elem.RoundTimesId;
                rounds.RoundTime = time.Times;
                rounds.RoundTypesId = elem.RoundTypesId;
                rounds.RoundType = type.Type;


                response.Add(rounds);
            }

            return response;
        }

        public List<RoundTimes> GetRoundTimes()
        {
            DataContext _context = new DataContext();

            var times =  _context.RoundTimes.ToList();
            if (!times.Any())
                return null;

            return times;

        }

        public List<RoundTypes> GetRoundTypes()
        {
            DataContext _context = new DataContext();

            var types =  _context.RoundTypes.ToList();
            if (!types.Any())
                return null;

            return types;

        }

        public RoundTimes GetRoundTimeRecord(int Id)
        {
            DataContext _context = new DataContext();

            var time =  _context.RoundTimes.FirstOrDefault(x => x.Id == Id);
            if (time == null)
                return null;

            return time;
        }

        public RoundTypes GetRoundTypeRecord(int Id)
        {
            DataContext _context = new DataContext();

            var type =  _context.RoundTypes.FirstOrDefault(x => x.Id == Id);
            if (type == null)
                return null;

            return type;

        }

        public Bus GetRoundBusRecord(int Id)
        {
            DataContext _context = new DataContext();

            var bus =  _context.Bus.FirstOrDefault(x => x.Id == Id && x.IsDeleted == 0);
            if (bus == null)
                return null;

            return bus;

        }

        public bool AddRoundPassenger(RoundPassengerDto roundPassengerDto)
        {
            DataContext _context = new DataContext();

            var passenger = new RoundPassengers();
            passenger.BusRoundId = roundPassengerDto.BusRoundId;
            passenger.ChildrenId = roundPassengerDto.ChildrenId;

            if (!UpdateBusAmount(roundPassengerDto.BusRoundId, "Add"))
                return false;

             _context.RoundPassengers.Add(passenger);
             _context.SaveChanges();

            return true;
        }

        public bool UpdateBusAmount(int roundId, string type)
        {
            DataContext _context = new DataContext();

            var busRound =  _context.BusRounds.FirstOrDefault(x => x.Id == roundId);
            if (busRound == null)
                return false;

            int currentAmount = busRound.CurrentAmount;
            int capacity = busRound.Capacity;
            int space = capacity - currentAmount;

            if (type == "Add" && space > 0)
            {
                busRound.CurrentAmount = currentAmount + 1;

            }
            else if (type == "delete" && currentAmount > 0)
            {
                busRound.CurrentAmount = currentAmount - 1;

            }
            else
            {

                return false;
            }


            _context.BusRounds.Update(busRound);
             _context.SaveChanges();

            return true;

        }

        public bool DeleteRoundPassenger(RoundPassengerDto roundPassengerDto)
        {
            DataContext _context = new DataContext();

            var record =  _context.RoundPassengers.FirstOrDefault(x => x.ChildrenId == roundPassengerDto.ChildrenId && x.BusRoundId == roundPassengerDto.BusRoundId);
            if (record == null)
                return false;

            if (!UpdateBusAmount(roundPassengerDto.BusRoundId, "delete"))
                return false;

            _context.RoundPassengers.Remove(record);
             _context.SaveChanges();

            return true;
        }

        public List<PassengersToReturnDto> GetRoundPassengers(int roundId)
        {
            DataContext _context = new DataContext();

            var passengers =  _context.RoundPassengers.Where(x => x.BusRoundId == roundId).ToList();
            if (passengers == null)
                return null;

            var repsonse = new List<PassengersToReturnDto>();


            foreach (var passenger in passengers)
            {
                var trip =  GetRoundRecord(passenger.BusRoundId);
                var children =  GetChildrenById(passenger.ChildrenId);
                var childrenImage =  PhotoServices.Instance.getUserImage("Children", passenger.ChildrenId);
                var parentInfo =  GetParentInfo(children.ParentsId);
                var childrenCase =  GetChildrenStatus(passenger.ChildrenId);
                 var childObj = new PassengersToReturnDto();

                if (trip.RoundTimesId == 1 && childrenCase.CaseId == 1 &&  IsChildrenVoucherValid(passenger.ChildrenId) == true)
                {
                    childObj.Id = children.Id;
                    childObj.Firstname = children.Firstname;
                    childObj.MiddleName = children.Middlename;
                    childObj.Lastname = children.Lastname;
                    childObj.Age = children.Age;
                    childObj.Lat = children.StartLat;
                    childObj.Lng = children.StartLong;
                    childObj.Address = children.StartAddress;
                    childObj.SchoolId = children.SchoolId;
                    childObj.Phonenumber = parentInfo.Phonenumber;
                    childObj.Url = childrenImage.Url;

                    repsonse.Add(childObj);



                };


                
                if(trip.RoundTimesId == 2 && childrenCase.CaseId == 3 &&  IsChildrenVoucherValid(passenger.ChildrenId) == true)
                {
                    childObj.Id = children.Id;
                    childObj.Firstname = children.Firstname;
                    childObj.MiddleName = children.Middlename;
                    childObj.Lastname = children.Lastname;
                    childObj.Age = children.Age;
                    childObj.Lat = children.EndLat;
                    childObj.Lng = children.EndLong;
                    childObj.Address = children.EndAddress;
                    childObj.SchoolId = children.SchoolId;
                    childObj.Phonenumber = parentInfo.Phonenumber;
                    childObj.Url = childrenImage.Url;

                    repsonse.Add(childObj);


                }

            }


            return repsonse;
        }

        public List<Bus> GetBuses()
        {
            DataContext _context = new DataContext();

            var buses =  _context.Bus.Where(x => x.IsDeleted == 0).ToList();
            if (!buses.Any())
                return null;

            return buses;
        }

        public List<Childrens> GetChildrens()
        {
            DataContext _context = new DataContext();

            var childrens =  _context.Childrens.Where(x => x.IsDeleted == 0).ToList();
            if (!childrens.Any())
                return null;

            return childrens;
        }

        public List<object> GetBusRoundsByTime(int busId, int timeId)
        {
            DataContext _context = new DataContext();

            var busRounds =  _context.BusRounds.Where(x => x.BusId == busId && x.IsDeleted == 0 && x.RoundTimesId == timeId).ToList();
            if (busRounds == null)
                return null;

            var response = new List<Object>();

            foreach (var elem in busRounds)
            {
                RoundTypes type =  GetRoundTypeRecord(elem.RoundTypesId);
                Bus bus =  GetRoundBusRecord(busId);
                RoundTimes time =  GetRoundTimeRecord(elem.RoundTimesId);
                var rounds = new BusRoundsProprties();

                rounds.Id = elem.Id;
                rounds.TripName = elem.TripName;
                rounds.BusId = elem.BusId;
                rounds.BusName = bus.Model;
                rounds.RoundTimesId = elem.RoundTimesId;
                rounds.RoundTime = time.Times;
                rounds.RoundTypesId = elem.RoundTypesId;
                rounds.RoundType = type.Type;


                response.Add(rounds);
            }

            return response;

        }

        public List<object> GetBusRoundsByType(int busId, int typeId)
        {
            DataContext _context = new DataContext();

            var busRounds =  _context.BusRounds.Where(x => x.BusId == busId && x.IsDeleted == 0 && x.RoundTypesId == typeId).ToList();
            if (busRounds == null)
                return null;

            var response = new List<Object>();

            foreach (var elem in busRounds)
            {
                RoundTypes type =  GetRoundTypeRecord(elem.RoundTypesId);
                Bus bus =  GetRoundBusRecord(busId);
                RoundTimes time =  GetRoundTimeRecord(elem.RoundTimesId);
                var rounds = new BusRoundsProprties();

                rounds.Id = elem.Id;
                rounds.TripName = elem.TripName;
                rounds.BusId = elem.BusId;
                rounds.BusName = bus.Model;
                rounds.RoundTimesId = elem.RoundTimesId;
                rounds.RoundTime = time.Times;
                rounds.RoundTypesId = elem.RoundTypesId;
                rounds.RoundType = type.Type;


                response.Add(rounds);
            }

            return response;

        }

        public BusRound GetRoundRecord(int roundId)
        {
            DataContext _context = new DataContext();

            var round =  _context.BusRounds.FirstOrDefault(x => x.Id == roundId);
            if (round == null)
                return null;

            return round;
        }

        public Childrens GetChildrenById(int childrenId)
        {
            DataContext _context = new DataContext();

            var children =  _context.Childrens.FirstOrDefault(x => x.Id == childrenId && x.IsDeleted == 0);
            if (children == null)
                return null;

            return children;
        }

        public User GetParentInfo(int parentsRecordId)
        {
            DataContext _context = new DataContext();

            var parentsRecord =  _context.Parents.FirstOrDefault(x => x.Id == parentsRecordId);
            if(parentsRecord == null)
                return null;

            var primaryInfo =  _context.Users.FirstOrDefault(x => x.Id == parentsRecord.PrimaryParent);
            if(primaryInfo == null)
                return null;

            return primaryInfo;
        }

        public ChildrensCases GetChildrenStatus(int childrenId)
        {
            DataContext _context = new DataContext();

            var childrenCase =  _context.ChildrensCases.FirstOrDefault(x => x.ChildrenId == childrenId);
            if(childrenCase == null)
                return null;

            return childrenCase;
        }

        public IEnumerable<RetunedSchools> GetSchoolsByRound(int roundId)
        {
            DataContext _context = new DataContext();


            var schools = new List<RetunedSchools>();
            var students =  GetRoundPassengers(roundId);
                
            foreach(var student in students)
            {
                var children =  GetChildrenById(student.Id);
                if(children == null)
                    return null;

                var school =  _context.Schools.FirstOrDefault(x => x.Id == children.SchoolId);
                    if(school == null)
                        return null;
                        
                 var obj = new RetunedSchools();

                obj.Id = school.Id;
                obj.Lat = school.Lat;
                obj.Lng = school.Long;
                obj.Area = school.Address;
                obj.Type = "School";
                obj.Name = school.SchoolName;

                schools.Add(obj);

            }

            var uiqueSchools = schools.GroupBy(p => p.Id).Select(grp => grp.First()).ToArray();


            return uiqueSchools;

        }

        public bool HandleChildrenStatus(List<request> requests)
        {
            DataContext _context = new DataContext();

            foreach (var obj in requests)
            {
                var children =  GetChildrenById(obj.i);
                string childName = children.Firstname + " " + children.Lastname;

                // checked means absence ,, unchecked means presence

                if (obj.t == "checked" && obj.s == "busPickUpAtHome")
                {

                    var notifcation =  AuthServices.Instance.SendNotifications(children.ParentsId, "absent/busPickUpAtHome", childName, null);
                    var track =  AppServices.Instance.TrackChildren(children.Id, 1);

                }
                else if(obj.t == "checked" && obj.s == "busDropAtSchool")
                {

                    var notifcation =  AuthServices.Instance.SendNotifications(children.ParentsId, "absent/busDropAtSchool", childName, null);
                    var track =  AppServices.Instance.TrackChildren(children.Id, 2);

                }
                else if(obj.t == "checked" && obj.s == "busPickUpAtSchool")
                {
                    var notifcation =  AuthServices.Instance.SendNotifications(children.ParentsId, "absent/busPickUpAtSchool", childName, null);
                    var track =  AppServices.Instance.TrackChildren(children.Id, 3);

                }
                else if(obj.t == "checked" && obj.s == "busDropAtHome")
                {

                    var notifcation =  AuthServices.Instance.SendNotifications(children.ParentsId, "absent/busDropAtHome", childName, null);
                    var track =  AppServices.Instance.TrackChildren(children.Id, 4);

                }
                
                else if (obj.t == "unchecked" && obj.s == "busPickUpAtHome")
                {

                    var notifcation =  AuthServices.Instance.SendNotifications(children.ParentsId, "with/busPickUpAtHome", childName, null);
                    var track =  AppServices.Instance.TrackChildren(children.Id, 2);

                }
                else if(obj.t == "unchecked" && obj.s == "busDropAtSchool")
                {

                    var notifcation =  AuthServices.Instance.SendNotifications(children.ParentsId, "with/busDropAtSchool", childName, null);
                    var track =  AppServices.Instance.TrackChildren(children.Id, 3);

                }
                else if(obj.t == "unchecked" && obj.s == "busPickUpAtSchool")
                {
                    var notifcation =  AuthServices.Instance.SendNotifications(children.ParentsId, "with/busPickUpAtSchool", childName, null);
                    var track =  AppServices.Instance.TrackChildren(children.Id, 4);

                }
                else if(obj.t == "unchecked" && obj.s == "busDropAtHome")
                {

                    var notifcation =  AuthServices.Instance.SendNotifications(children.ParentsId, "with/busDropAtHome", childName, null);
                    var track =  AppServices.Instance.TrackChildren(children.Id, 1);

                }else{

                    return false;
                }


            };

            return true;
        }

        public bool HandleBusTrip(int busId, string type)
        {
            DataContext _context = new DataContext();

            var bus =  _context.Bus.FirstOrDefault(x => x.Id == busId && x.IsDeleted == 0);
            if(bus == null)
                return false;
            if(type == "Start")
            {

                  bus.Status = "On";

            }
            if(type == "End")
            {

                  bus.Status = "Off";

            }

            _context.Bus.Update(bus);
             _context.SaveChanges();
            return true;
        }

        public List<object> GetChildrensBySchool(int roundId, int schoolId)
        {
            DataContext _context = new DataContext();

            dynamic obj;
            var array = new List<object>();
            string Url;

            var roundPassengers =  GetRoundPassengers(roundId);

            foreach(var child in roundPassengers)
            {
                if(child.SchoolId == schoolId)
                {                
                    
                    var image =  PhotoServices.Instance.getUserImage("Children", child.Id);
                    if(image == null)
                    {
                        Url = null;
                    }else{

                        Url = image.Url;

                    }

                    obj = new {
                        Id = child.Id,
                        Name= child.Firstname + " " + child.Lastname,
                        Lat = child.Lat,
                        Lng = child.Lng,
                        Area = child.Address,
                        Type = "Children",
                        Phonenumber = child.Phonenumber,
                        Url = Url
                    };

                    array.Add(obj);

                }

            }

            return array;
            
        }

        public IEnumerable<object> ViewAllRoundPassengers(int roundId)
        {
            DataContext _context = new DataContext();

            var passengers =  _context.RoundPassengers.Where(x => x.BusRoundId == roundId).ToList();
            if (passengers == null)
                return null;

            var repsonse = new List<PassengersToReturnDto>();


            foreach (var passenger in passengers)
            {
                var trip =  GetRoundRecord(passenger.BusRoundId);
                var children =  GetChildrenById(passenger.ChildrenId);
                var childrenImage =  PhotoServices.Instance.getUserImage("Children", passenger.ChildrenId);
                var parentInfo =  GetParentInfo(children.ParentsId);
                var childrenCase =  GetChildrenStatus(passenger.ChildrenId);
                 var childObj = new PassengersToReturnDto();

                    childObj.Id = children.Id;
                    childObj.Firstname = children.Firstname;
                    childObj.MiddleName = children.Middlename;
                    childObj.Lastname = children.Lastname;
                    childObj.Age = children.Age;
                    childObj.Lat = children.StartLat;
                    childObj.Lng = children.StartLong;
                    childObj.Address = children.StartAddress;
                    childObj.SchoolId = children.SchoolId;
                    childObj.Phonenumber = parentInfo.Phonenumber;
                    childObj.Url = childrenImage.Url;

                    repsonse.Add(childObj);

            }


            return repsonse;
        }

        public bool IsChildrenVoucherValid(int childrenId)
        {
            DataContext _context = new DataContext();

            var childrenVoucher =  _context.ChildrensVouchers.FirstOrDefault(x => x.ChildrenId == childrenId);
            if(childrenVoucher == null)
                return false;

            var timeLeft = childrenVoucher.ExpireDate - DateTime.Now;
            int daysLeft = timeLeft.Days;
            if(daysLeft <= 0)
                return false;

            return true;
        }
    }
    
}

