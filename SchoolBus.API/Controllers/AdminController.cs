using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using HafelatyServices;
using HafelatyServices.Dtos;
using HafelatyServices.Dtos.ToReturnDto;
using HafelatyEntites;


namespace SchoolBus.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class AdminController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        public AdminController(IConfiguration config, DataContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
            _config = config;

        }


        [HttpPost("addschool")]
        public ActionResult AddSchool(SchoolToAddDto schoolDto)
        {


            if (AdminServices.Instance.SchoolExists(schoolDto.SchoolName))
                return BadRequest(new { message = "School already exists" });

            var schoolToCreate = new Schools
            {
                SchoolName = schoolDto.SchoolName,
                Address = schoolDto.Address,
                Phonenumber = schoolDto.Phonenumber,
                Lat = schoolDto.Lat,
                Long = schoolDto.Long
            };

            var createdSchool =  AdminServices.Instance.AddSchool(schoolToCreate);

            return Ok(new
            {

                message = "Success"

            });

        }

        [HttpPost("editschool")]
        public ActionResult EditSchool(SchoolToUpdateDto schoolDto)
        {

            var updatedSchool =  AdminServices.Instance.EditSchool(schoolDto);

            if (updatedSchool == null)
                return BadRequest(new { message = "School Doesnt Exist" });

            return Ok(new
            {

                message = "Success"

            });

        }

        [HttpPost("deleteschool")]
        public ActionResult DeleteSchool(int id)
        {

            var deletedSchool =  AdminServices.Instance.DeleteSchool(id);

            if (deletedSchool == null)
                return BadRequest(new { message = "School Doesnt Exist" });

            return Ok(new
            {

                message = "Success"

            });

        }

        [HttpPost("createVoucher")]

        public ActionResult CreateCards(CreateCardsDto createCardsDto)
        {
            if ( AdminServices.Instance.CreateVouchers(createCardsDto))
                return Ok(new { message = "Success" });

            return BadRequest(new { message = "Something went Wrong" });
        }

        [HttpGet("getVouchers")]

        public ActionResult GetCards()
        {

            var vouchers =  AdminServices.Instance.GetCards();
            var vouchersToReturn = _mapper.Map<IEnumerable<VouchersToReturn>>(vouchers);


            return Ok(vouchersToReturn);

        }

        [HttpPost("addBus")]

        public ActionResult AddBus(BusToAddDto busToAddDto)
        {

            var busToAdd = new Bus
            {
                DriverId = busToAddDto.DriverId,
                Capacity = busToAddDto.Capacity,
                Area = busToAddDto.Area,
                Model = busToAddDto.Model,
                Plate = busToAddDto.Plate,
                Licenses = busToAddDto.Licenses,
                Status = "Off"
            };

            var createdBus =  AdminServices.Instance.AddBus(busToAdd);



            return Ok(createdBus);

        }

        [HttpPost("editBus")]
        public ActionResult UpdateBus(BusToUpdate busToUpdate)
        {
            var editBus =  AdminServices.Instance.EditBus(busToUpdate);

            if (editBus == null)
                return BadRequest(new { message = "Bus Doesnt Exists" });

            return Ok(
                new
                {
                    message = "Done"
                }
            );
        }

        [HttpPost("deleteBus")]
        public ActionResult DeleteBus(BusToDeleteDto busToDeleteDto)
        {
            var editBus =  AdminServices.Instance.DeleteBus(busToDeleteDto);

            if (editBus == null)
                return BadRequest(new { message = "Bus Doesnt Exists" });

            return Ok(
                new
                {
                    message = "Done"
                }
            );
        }

        [HttpGet("getAllBus")]

        public ActionResult GetAllBus()
        {

            var buses =  AdminServices.Instance.GetAllBus();
            var busesToReturn = _mapper.Map<IEnumerable<BusesToReturn>>(buses);


            return Ok(busesToReturn);

        }

        [HttpGet("getBus/{id}")]

        public ActionResult GetBus(int id)
        {

            var bus =  AdminServices.Instance.GetBus(id);
            if (bus == null)
                return BadRequest(new { message = "Bus Doesnt Exists" });


            var busToReturn = _mapper.Map<BusesToReturn>(bus);


            return Ok(busToReturn);

        }

        [HttpGet("getAvailableDrivers")]

        public ActionResult GetAvailableDrivers()
        {
            var drivers =  AdminServices.Instance.GetAvailableDrivers();

            if (drivers == null)
                return Ok(new { message = "empty" });

            // var driversInfo = _auth.GetUserInfo(drivers);

            var driversToReturn = _mapper.Map<IEnumerable<DriverToReturnDto>>(drivers);

            return Ok(driversToReturn);
        }

        [HttpGet("getAvailableChildrens")]

        public ActionResult GetAvailableChildrens()
        {
            var childrens =  AdminServices.Instance.GetAvailableChildrens();

            if (childrens == null)
                return Ok(new { message = "empty" });

            var childrensToReturn = _mapper.Map<IEnumerable<ChildToReturnDto>>(childrens);

            return Ok(childrensToReturn);
        }

        [HttpPost("addPassenger")]

        public ActionResult AddPassenger(PassengersDto passengersDto)
        {
            if (! AdminServices.Instance.UpdateBusAmounts(passengersDto.BusId, passengersDto.ChildrenId))
                return BadRequest(new { message = "Something Went Wrong" });

            var passengerToAdd = new Passengers
            {
                BusId = passengersDto.BusId,
                ChildrenId = passengersDto.ChildrenId
            };

            var passenger =  AdminServices.Instance.AddPassengers(passengerToAdd);

            return Ok(new { message = "Successfully Added" });
        }

        [HttpGet("getAvailableBuses")]

        public ActionResult GetAvailableBuses()
        {

            var buses =  AdminServices.Instance.GetAvailableBuses();
            var busesToReturn = _mapper.Map<IEnumerable<BusesAmountsToReturn>>(buses);



            return Ok(busesToReturn);

        }

        [AllowAnonymous]
        [HttpPost("addAdvertisment")]

        public ActionResult addAdvertisment(IFormFile file, [FromForm] string Name,[FromForm] string Descreption)
        {


            if (file == null || file.Length == 0)
                return BadRequest(new { message = "Please Choose Image" });

            if ( PhotoServices.Instance.isAdvNameUsed(Name))
                return BadRequest(new { message = "Name already exists" });

            var folderName = Path.Combine("Resources", "Advertisments");
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            string extension = System.IO.Path.GetExtension(file.FileName);

            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            string nameToUse = Name;
            nameToUse = nameToUse.Replace(" ", String.Empty);


            var uniqueFileName = $"{nameToUse}_adv{extension}";
            var dbPath = Path.Combine(folderName, uniqueFileName);

            using (var fileStream = new FileStream(Path.Combine(filePath, uniqueFileName), FileMode.Create))
            {
                 file.CopyToAsync(fileStream);
            }

            var Url = $"http://www.hafelaty.com/{dbPath}";

            var replacedUrl = Url.Replace(@"\","/");


            var advToAdd = new Advertisment
            {
                Name = Name,
                Descreption = Descreption,
                Url = replacedUrl
            };

            var adv =  PhotoServices.Instance.addAdvertisment(advToAdd);

            return Ok(replacedUrl);
        }

        [AllowAnonymous]
        [HttpPost("editAdv")]
        public ActionResult EditAdv(IFormFile file, [FromForm] string Name, [FromForm] string Descreption, [FromForm] int Id)
        {

            if (file == null || file.Length == 0)
                return BadRequest(new { message = "Please Choose Image" });

            if ( PhotoServices.Instance.isAdvNameUsed(Name))
                return BadRequest(new { message = "Name already exists" });

            var folderName = Path.Combine("Resources", "Advertisments");
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            string extension = System.IO.Path.GetExtension(file.FileName);

            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            string nameToUse = Name;
            nameToUse = nameToUse.Replace(" ", String.Empty);


            var uniqueFileName = $"{nameToUse}_adv{extension}";
            var dbPath = Path.Combine(folderName, uniqueFileName);

            using (var fileStream = new FileStream(Path.Combine(filePath, uniqueFileName), FileMode.Create))
            {
                 file.CopyToAsync(fileStream);
            }

            var Url = $"http://www.hafelaty.com/{dbPath}";

            var replacedUrl = Url.Replace(@"\","/");


            var editAdv =  PhotoServices.Instance.EditAdv(Name, Descreption, Id, replacedUrl);

            if (editAdv == null)
                return BadRequest(new { message = "Adv Doesnt Exists" });

            return Ok(new{message = "Done"});
        }

        [AllowAnonymous]
        [HttpPost("deleteAdv")]
        public ActionResult DeleteAdv(CommonIdDto commonIdDto)
        {

           
            var deleteAdv =  PhotoServices.Instance.DeleteAdv(commonIdDto);

            if (deleteAdv == null)
                return BadRequest(new { message = "Adv Doesnt Exists" });

            return Ok(new{message = "Done"});
        }

        [AllowAnonymous]
        [HttpPost("addOffer")]
        public ActionResult AddOffer(IFormFile file, [FromForm] string Hyperlink)
        {


            if (file == null || file.Length == 0)
                return BadRequest(new { message = "Please Choose Image" });


            var folderName = Path.Combine("Resources", "Hotoffers");
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            string extension = System.IO.Path.GetExtension(file.FileName);

            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            string nameToUse = file.FileName;
            nameToUse = nameToUse.Replace(" ", String.Empty);


            var uniqueFileName = $"{nameToUse}_offers{extension}";
            var dbPath = Path.Combine(folderName, uniqueFileName);

            using (var fileStream = new FileStream(Path.Combine(filePath, uniqueFileName), FileMode.Create))
            {
                 file.CopyToAsync(fileStream);
            }

            var Url = $"http://www.hafelaty.com/{dbPath}";

            var replacedUrl = Url.Replace(@"\","/");


            var offerToAdd = new Offers
            {
                HyperLink = Hyperlink,
                Url = replacedUrl
            };

            var offer =  PhotoServices.Instance.addOffer(offerToAdd);

            return Ok(offer);
        }

        [AllowAnonymous]
        [HttpPost("deleteOffer")]
        public ActionResult DeleteOffer(CommonIdDto commonIdDto)
        {

           
            var deleteOffer =  PhotoServices.Instance.DeleteOffer(commonIdDto);

            if (deleteOffer == null)
                return BadRequest(new { message = "Offer Doesnt Exists" });

            return Ok(new{message = "Done"});
        }

        [AllowAnonymous]
        [HttpGet("getAddressRequests")]
        public ActionResult GetAddressRequests()
        {

            var response = new List<Object>();
            var obj = new Object();

           
            var requests =  AdminServices.Instance.GetAddressRequests();

            if (requests == null)
                return BadRequest(new { message = "No Requests Yet" });

            foreach(var elem in requests){

                var children =  AdminServices.Instance.getChildInfo(elem.ChildrenId);
                // var action =  AdminServices.Instance.getActionInfo(elem.ActionTypeId);
                string childrenName = children.Firstname + " " + children.Lastname;
                // int childrenId = children.Id;
                // int actionId = action.Id;
                // string actionName = action.By;

                 obj = new 
                 {
                     Id = elem.Id,
                     ChildrenId = elem.ChildrenId,
                     ChildrenName = childrenName,
                     StartLat = elem.StartLat,
                     StartLong = elem.StartLong,
                     StartAddress = elem.StartAddress,
                     EndLat = elem.EndLat,
                     EndLong = elem.EndLong,
                     EndAddress = elem.EndAddress 

                };

            response.Add(obj);

            }

            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost("RecieveRequest")]
        public ActionResult RecieveRequest(RecieveRequestDto recieveRequestDto)
        {
            var requestToSend =  AdminServices.Instance.RecieveRequest(recieveRequestDto.Id, recieveRequestDto.Status);
                if(requestToSend == null)
                    return BadRequest(new {message = "Something Went Wrong"});

            return Ok(new {message = requestToSend});

        }







    }
}
