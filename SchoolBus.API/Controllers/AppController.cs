using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Firebase.Database;
using Firebase.Database.Query;
using Newtonsoft.Json;
using System.Device.Location;
using HafelatyServices;
using HafelatyServices.Dtos;
using HafelatyServices.Dtos.ToReturnDto;
using HafelatyEntites;


namespace SchoolBus.API.Controllers
{

    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class AppController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;
        private readonly DataContext _context;
        public AppController(IConfiguration config, IMapper mapper, DataContext context)
        {
            _mapper = mapper;
            _config = config;
            _context = context;

        }



        // [AllowAnonymous]
        [HttpPost("addparent")]
        public ActionResult AddParents(AddSecondaryParentDto userForRegisterDto)
        {



            if ( AuthServices.Instance.UserExists(userForRegisterDto.Phonenumber))
                return BadRequest(new { message = "Phonenumber already exists" });

            if ( AppServices.Instance.SecondaryFull(userForRegisterDto.ParentId))
                return BadRequest(new { message = "Full" });



            var userToCreate = new User
            {
                Username = userForRegisterDto.Username,

                // Email = userForRegisterDto.Email,

                UserTypeId = userForRegisterDto.UserTypeId,

                Phonenumber = userForRegisterDto.Phonenumber,


            };

            var createdUser =  AuthServices.Instance.Register(userToCreate, userForRegisterDto.Password);

            var userToCreateInfo = new UserInfo
            {
                Firstname = userForRegisterDto.Firstname,

                Middlename = userForRegisterDto.Middlename,

                Lastname = userForRegisterDto.Lastname,

                Age = userForRegisterDto.Age,

                UserId = createdUser.Id,

                GenderId = userForRegisterDto.GenderId

            };

            var creatdInfo =  AuthServices.Instance.AddInfo(userToCreateInfo);

            if ( AppServices.Instance.IsSecondaryFirstExists(userForRegisterDto.ParentId))
            {

                var createdSecondary =  AppServices.Instance.AddSecondParent(userForRegisterDto.ParentId, createdUser.Id);
                return Ok(new { message = createdUser.Id.ToString() });



            }
            else
            {

                var createdFirstSecondary =  AppServices.Instance.AddFirstParent(userForRegisterDto.ParentId, createdUser.Id);
                return Ok(new { message = createdUser.Id.ToString() });



            }



        }

        [AllowAnonymous]
        [HttpGet("getparents/{id}")]

        public ActionResult GetParents(int id)
        {


            int? counter = null;


            var parents =  _context.Parents.FirstOrDefault(x => x.PrimaryParent == id);
            if (parents == null)
                return BadRequest(new { counter = 0, message = "empty" });

            var firstParent =  AuthServices.Instance.GetUserInfo(parents.fSecondaryParent);
            var secondParent =  AuthServices.Instance.GetUserInfo(parents.SSecondaryParet);

            if (firstParent == null && secondParent != null)
            {
                var userSParent =  AuthServices.Instance.GetUser(parents.SSecondaryParet);
                counter = 1;
                var image =  PhotoServices.Instance.getUserImage("User", parents.SSecondaryParet);
                var Url = image.Url;
                var secondParentInfo = new
                {

                    Firstname = secondParent.Firstname,
                    Middlename = secondParent.Middlename,
                    Lastname = secondParent.Lastname,
                    Phonenumber = userSParent.Phonenumber,
                    // Email = userSParent.Email,
                    Username = userSParent.Username,
                    UserType = userSParent.UserTypeId,
                    UserId = parents.SSecondaryParet,
                    Url = Url

                };

                return Ok(new
                {
                    counter = counter,
                    firstParent = secondParentInfo




                });

            }
            else if (firstParent != null && secondParent == null)
            {

                var userFParent =  AuthServices.Instance.GetUser(parents.fSecondaryParent);
                var image =  PhotoServices.Instance.getUserImage("User", parents.fSecondaryParent);
                var Url = image.Url;
                var firstParentInfo = new
                {

                    Firstname = firstParent.Firstname,
                    Middlename = firstParent.Middlename,
                    Lastname = firstParent.Lastname,
                    Phonenumber = userFParent.Phonenumber,
                    // Email = userFParent.Email,
                    Username = userFParent.Username,
                    UserType = userFParent.UserTypeId,
                    UserId = parents.fSecondaryParent,
                    Url = Url

                };

                counter = 1;

                return Ok(new
                {
                    counter = counter,
                    firstParent = firstParentInfo

                });

            }
            else if (firstParent != null && secondParent != null)
            {

                var userFParent =  AuthServices.Instance.GetUser(parents.fSecondaryParent);
                var userSParent =  AuthServices.Instance.GetUser(parents.SSecondaryParet);
                var firstImage =  PhotoServices.Instance.getUserImage("User", parents.fSecondaryParent);
                var FirstUrl = firstImage.Url;
                var secondImage =  PhotoServices.Instance.getUserImage("User", parents.SSecondaryParet);
                var SecondUrl = secondImage.Url;


                var firstParentInfo = new
                {

                    Firstname = firstParent.Firstname,
                    Middlename = firstParent.Middlename,
                    Lastname = firstParent.Lastname,
                    Phonenumber = userFParent.Phonenumber,
                    // Email = userFParent.Email,
                    Username = userFParent.Username,
                    UserType = userFParent.UserTypeId,
                    UserId = parents.fSecondaryParent,
                    Url = FirstUrl

                };

                var secondParentInfo = new
                {

                    Firstname = secondParent.Firstname,
                    Middlename = secondParent.Middlename,
                    Lastname = secondParent.Lastname,
                    Phonenumber = userSParent.Phonenumber,
                    // Email = userSParent.Email,
                    Username = userSParent.Username,
                    UserType = userSParent.UserTypeId,
                    UserId = parents.SSecondaryParet,
                    Url = SecondUrl

                };



                counter = 2;
                return Ok(new
                {
                    counter = counter,
                    firstParent = firstParentInfo,
                    secondParent = secondParentInfo

                });
            }

            else
            {
                var message = new
                {

                    message = "Empty"

                };

                return Ok(new
                {
                    counter = 0,
                    message = message
                });

            }






        }

        [HttpPost("deleteparent")]
        public ActionResult Delete(ParentToDeleteDto parentToDelete)
        {



            var deletedParent =  AppServices.Instance.Delete(parentToDelete);

            if (deletedParent == null)
                return BadRequest(new { message = "Parent Doesnt Exist" });

            return Ok(new
            {

                message = "Success"

            });


        }

        [HttpPost("voucherIsValid")]

        public ActionResult VoucherIsValid(VoucherDto code)
        {
            if ( AppServices.Instance.VoucherIsValid(code))
            {
                return Ok(new { message = "Yes" });

            }
            else
            {

                return Ok(new { message = "No" });
            }
        }

        [HttpPost("addChild")]
        public ActionResult AddChild(ChildToAddDto childToAddDto)
        {

            var parentsRecord =  AppServices.Instance.GetParentsRecord(childToAddDto.ParentId);
            if (parentsRecord == null)
                return BadRequest(new { message = "Parent Doesnt Exisit" });


            if ( AppServices.Instance.GetChildByName(childToAddDto.Firstname, childToAddDto.Middlename, childToAddDto.Lastname))
                return BadRequest(new { message = "Child Already Exisit" });

            if (! AppServices.Instance.ValidateVoucher(childToAddDto.Code))
                return BadRequest(new { message = "Voucher Is Used" });


            var childToAdd =  AppServices.Instance.AddChild(childToAddDto, parentsRecord.Id);
            var track =  AppServices.Instance.TrackChildren(childToAdd.Id, 1);

            if( AppServices.Instance.UseVoucherForChildren(childToAdd.Id, childToAddDto.Code))
            {
                    var child = new
                        {
                            Id = childToAdd.Id,
                            Firstname = childToAddDto.Firstname,
                            Middlename = childToAddDto.Middlename,
                            Lastname = childToAddDto.Lastname,
                            Age = childToAddDto.Age,
                            SchoolId = childToAddDto.SchoolId


                        };

                        return Ok(new {message = "Created", Child = child });

            }else{
                        return BadRequest(new {message = "Something Went Wrong!"});

            }




        }

        // [HttpGet("getChilds/{id}")]

        // public ActionResult GetChilds(int id)
        // {

        //     var response = new List<Object>();

        //     var parentsRecord =  AppServices.Instance.GetParentsRecord(id);
        //     if (parentsRecord == null)
        //         return BadRequest(new { message = "Parent Doesnt Exisit" });

        //     var childrens =  AppServices.Instance.GetChildsByParent(parentsRecord.Id);
        //     if (childrens == null)
        //         return BadRequest(new { message = "No Childs For This Parent" });

        //     foreach (var elem in childrens)
        //     {

        //         var image =  PhotoServices.Instance.getUserImage("Children", elem.Id);
        //         var Url = image.Url;
        //         dynamic obj;


        //         if (elem.Passengers != null)
        //         {

        //             obj = new
        //             {

        //                 Id = elem.Id,
        //                 Firstname = elem.Firstname,
        //                 Middlename = elem.Middlename,
        //                 Lastname = elem.Lastname,
        //                 Age = elem.Age,
        //                 StartLat = elem.StartLat,
        //                 StartLong = elem.StartLong,
        //                 StartAddress = elem.StartAddress,
        //                 EndLat = elem.EndLat,
        //                 EndLong = elem.EndLong,
        //                 EndAddress = elem.EndAddress,
        //                 SchoolId = elem.SchoolId,
        //                 Attendance = elem.Passengers.Attendance,
        //                 Url = Url

        //             };
        //         }
        //         else
        //         {


        //             obj = new
        //             {

        //                 Id = elem.Id,
        //                 Firstname = elem.Firstname,
        //                 Middlename = elem.Middlename,
        //                 Lastname = elem.Lastname,
        //                 Age = elem.Age,
        //                 StartLat = elem.StartLat,
        //                 StartLong = elem.StartLong,
        //                 StartAddress = elem.StartAddress,
        //                 EndLat = elem.EndLat,
        //                 EndLong = elem.EndLong,
        //                 EndAddress = elem.EndAddress,
        //                 SchoolId = elem.SchoolId,
        //                 Attendance = 2,
        //                 Url = Url


        //             };

        //         }

        //         response.Add(obj);
        //     }

        //     // var ChildernsToReturn = _mapper.Map<IEnumerable<ChildToReturnDto>>(childrens);


        //     return Ok(response);

        // }

        [HttpPost("deleteChild")]

        public ActionResult DeleteChild(ChildToDeleteDto childToDeleteDto)
        {

            var childToDelete =  AppServices.Instance.DeleteChild(childToDeleteDto);
            if (childToDelete == null)
                return BadRequest(new { message = "No Child" });

            return Ok(new { message = "Success" });


        }

        [HttpPost("updateVoucher")]

        public ActionResult UpdateVoucher(UpdateVoucherDto updateVoucherDto)
        {

            if (! AppServices.Instance.ValidateVoucher(updateVoucherDto.Code))
                return BadRequest(new { message = "Voucher Is Used" });

            if(! AppServices.Instance.UseVoucherForChildren(updateVoucherDto.ChildrenId, updateVoucherDto.Code))
                return BadRequest(new { message = "Something Went Wrong" });

            return Ok(new { message = "Success" });


        }

        [HttpPost("editChild")]

        public ActionResult EditChild(ChildToUpdateDto childToUpdateDto)
        {
            var childToUpdate =  AppServices.Instance.EditChild(childToUpdateDto);
            if (childToUpdate == null)
                return BadRequest(new { message = "No Child" });

            return Ok(new { message = "Success" });
        }

        [HttpPost("updateChildAddress")]

        public ActionResult UpdateChildAddress(updateAddressDto addressToAddDto)
        {
            var childAddress =  AppServices.Instance.AddAddressToChild(addressToAddDto);
            if (childAddress == null)
                return BadRequest(new { message = "Child Doesnt Exists" });

            return Ok(new { message = "Done" });
        }



        [HttpGet("getChildAddress/{id}")]
        public ActionResult getChildAddress(int Id)
        {
            var childAddress =  AppServices.Instance.getChildAddress(Id);
            if (childAddress == null)
                return BadRequest(new { message = "Child Doesnt Exists" });


            var childrensToReturn = _mapper.Map<ChildToReturnDto>(childAddress);

            return Ok(childrensToReturn);

        }

        [AllowAnonymous]
        [HttpGet("getAds")]
        public ActionResult GetAds()
        {

            var ads =  PhotoServices.Instance.GetAds();
            if (ads == null)
                return BadRequest(new { message = "No Ads Yet" });

            // var schoolsToReturn = _mapper.Map<IEnumerable<SchoolsToReturnDto>>(schools);

            return Ok(ads);

        }


        [HttpGet("getAdmins/{type}")]
        public ActionResult GetAdmins(int type)
        {
            List<AdminsToReturnDto> info = new List<AdminsToReturnDto>();

            // type 1 is admin // type 2 is managment

            var admins =  AppServices.Instance.getAdmins(type);
            if (admins == null)
                return BadRequest(new { message = "No Admins to chat with" });

            foreach (var elem in admins)
            {

                info.Add(AppServices.Instance.GetAdminInfo(elem.Id));

            }

            // var adminsToReturn = _mapper.Map<AdminsToReturnDto>(admins);

            return Ok(info);

        }

        [AllowAnonymous]
        [HttpGet("getOffers")]
        public ActionResult GetOffers()
        {

            var offers =  PhotoServices.Instance.GetOffers();
            if (offers == null)
                return BadRequest(new { message = "No Offers Yet" });

            // var schoolsToReturn = _mapper.Map<IEnumerable<SchoolsToReturnDto>>(schools);

            return Ok(offers);

        }

        [AllowAnonymous]
        [HttpGet("getChilds/{id}")]
        public async Task<IActionResult> GetChilds(int Id)
        {

            var parentsRecord =  _context.Parents.FirstOrDefault(x => x.fSecondaryParent == Id || x.SSecondaryParet == Id || x.PrimaryParent == Id);
                if(parentsRecord == null)
                     return BadRequest(new { message = "Parent Doesnt Exisit" });
            var childrens =  _context.Childrens.Where(x => x.ParentsId == parentsRecord.Id && x.IsDeleted == 0).ToList();
                if(!childrens.Any())
                     return BadRequest(new { message = "No Childs For This Parent" });


            var buses = new List<Object>();
            dynamic obj;
            int? noVlaue = null;


            foreach (var elem in childrens)
            {
                string SchoolName;
                BusRound record = new BusRound();
                string expireyTime;

                var childrenVoucher =  AppServices.Instance.GetChildrenVouchersRecord(elem.Id);
                if(childrenVoucher == null)
                {
                     expireyTime = "";   
                }else{

                    var timeLeft = childrenVoucher.ExpireDate - DateTime.Now; 
                    if(timeLeft.Days > 0)
                    {
                        string daysLeft = timeLeft.Days.ToString(); 
                        expireyTime = daysLeft + " Days Left"; 

                    }else{

                        expireyTime = "Voucher Is Expired";
                    }


                }

                var round =  _context.RoundPassengers.FirstOrDefault(x => x.ChildrenId == elem.Id);
                if(round != null)
                {
                     record =  _context.BusRounds.FirstOrDefault(x => x.Id == round.BusRoundId);
                }else{
                    record = null;
                }
                var image =  PhotoServices.Instance.getUserImage("Children", elem.Id);
                var school =  _context.Schools.FirstOrDefault(x => x.Id == elem.SchoolId);
                 if(school == null){
                      SchoolName = "";
                 }else{
                      SchoolName = school.SchoolName;
                 }
                string Url;
                if(image == null)
                {
                        Url = "";
                }else{

                        Url = image.Url;

                }
                if(record != null)
                {
                    var bus =  _context.Bus.FirstOrDefault(x => x.Id == record.BusId);

                    var firebaseClient = new FirebaseClient("https://hafelah.firebaseio.com/");

                    var dbLogins =  await firebaseClient
                                           .Child("Bus")
                                           .Child(record.BusId.ToString())
                                           .OnceAsync<string>();

                    if(dbLogins.Any() && elem.StartLat != null && bus.Status == "On"){

                            double childLat = Convert.ToDouble(elem.StartLat);
                            double childLng = Convert.ToDouble(elem.StartLong);
                            double busLat = Convert.ToDouble(dbLogins.FirstOrDefault(x => x.Key == "Driver_lat")?.Object);
                            double busLng = Convert.ToDouble(dbLogins.FirstOrDefault(x => x.Key == "Driver_long")?.Object);
                            // string tripStatus = dbLogins.FirstOrDefault(x => x.Key == "StartTrip")?.Object;

                            var childLocation = new GeoCoordinate(childLat, childLng);
                            var busLocation = new GeoCoordinate(busLat, busLng);
                            double distance = childLocation.GetDistanceTo(busLocation); // metres
                            double distanceInKm = distance / 1000;
                            int speed = 30;
                            double time = distanceInKm / speed;
                            var interval = TimeSpan.FromHours(time);
                            var hours = interval.Hours;
                            var minutes = interval.Minutes;
                            // string hourMinute = interval.ToString("HH:mm");
                            // var zeroSecondDate = interval.AddSeconds(-interval.Second);
                            var returnedTime = $"{hours}:{minutes}";


                            obj = new
                            {   
                                Id = elem.Id,
                                Firstname = elem.Firstname,
                                Middlename = elem.Middlename,
                                Lastname = elem.Lastname,
                                Age = elem.Age,
                                StartLat = elem.StartLat,
                                StartLong = elem.StartLong,
                                StartAddress = elem.StartAddress,
                                EndLat = elem.EndLat,
                                EndLong = elem.EndLong,
                                EndAddress = elem.EndAddress,
                                SchoolId = elem.SchoolId,
                                SchoolName = SchoolName,
                                Attendance = round.Attendance,
                                Url = Url,
                                time = returnedTime,
                                status = bus.Status,
                                bus = record.BusId,
                                trip = bus.Trip,
                                expireyTime = expireyTime
                                
                            };
                            buses.Add(obj);


                    }else{
                            
                        obj = new
                        {   
                                Id = elem.Id,
                                Firstname = elem.Firstname,
                                Middlename = elem.Middlename,
                                Lastname = elem.Lastname,
                                Age = elem.Age,
                                StartLat = elem.StartLat,
                                StartLong = elem.StartLong,
                                StartAddress = elem.StartAddress,
                                EndLat = elem.EndLat,
                                EndLong = elem.EndLong,
                                EndAddress = elem.EndAddress,
                                SchoolId = elem.SchoolId,
                                SchoolName = SchoolName,
                                Attendance = round.Attendance,
                                Url = Url,
                                time = noVlaue,
                                status = "Off",
                                bus = record.BusId,
                                trip = bus.Trip,
                                expireyTime = expireyTime




                        };
                        buses.Add(obj);

                    }


                }else{
                        obj = new
                        {   
                            Id = elem.Id,
                            Firstname = elem.Firstname,
                            Middlename = elem.Middlename,
                            Lastname = elem.Lastname,
                            Age = elem.Age,
                            StartLat = elem.StartLat,
                            StartLong = elem.StartLong,
                            StartAddress = elem.StartAddress,
                            EndLat = elem.EndLat,
                            EndLong = elem.EndLong,
                            EndAddress = elem.EndAddress,
                            SchoolId = elem.SchoolId,
                            SchoolName = SchoolName,
                            Attendance = 2,
                            Url = Url,
                            time = noVlaue,
                            status = "Off",
                            bus = noVlaue,
                            trip = noVlaue,
                            expireyTime = expireyTime




                        };
                        buses.Add(obj);
                }
                



                   
            };

            return Ok(buses);

        }





    }
}