using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CorePush.Google;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using HafelatyServices;
using HafelatyServices.Dtos;
using HafelatyServices.Dtos.ToReturnDto;
using HafelatyEntites;

namespace SchoolBus.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly DataContext _context;
        public AuthController(IConfiguration config, DataContext context)
        {
            _config = config;
            _context = context;

        }

        [AllowAnonymous]
        [HttpPost("register")]
        public ActionResult Register(UserForRegisterDto userForRegisterDto)
        {
            // validate request 


            // userForRegisterDto.Username = userForRegisterDto.Username.ToLower();

            if ( AuthServices.Instance.UserExists(userForRegisterDto.Phonenumber))
                return BadRequest(new { message = "Phonenumber already exists" });

            var userToCreate = new User
            {
                Username = userForRegisterDto.Username,

                // Email = userForRegisterDto.Email,

                Phonenumber = userForRegisterDto.Phonenumber,

                UserTypeId = userForRegisterDto.UserTypeId,

                DeviceToken = userForRegisterDto.DeviceToken

            };

            var createdUser =  AuthServices.Instance.Register(userToCreate, userForRegisterDto.Password);

            var userToCreateInfo = new UserInfo
            {
                Firstname = userForRegisterDto.Firstname,

                Middlename = userForRegisterDto.Middlename,

                Lastname = userForRegisterDto.Lastname,

                Age = userForRegisterDto.Age,

                UserId = createdUser.Id,

                GenderId = userForRegisterDto.GenderId,

            };

            var creatdInfo =  AuthServices.Instance.AddInfo(userToCreateInfo);

            if (userForRegisterDto.UserTypeId == 2)
            {

                var createParentRecord =  AppServices.Instance.addParentRecord(createdUser.Id);

            }


            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, createdUser.Id.ToString()),
                new Claim(ClaimTypes.Name, createdUser.Username)

            };


            var key = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(_config.GetSection("AppSettings:Token").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = System.DateTime.Now.AddDays(365),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDescriptor);

            var image =  PhotoServices.Instance.getUserImage("User", userToCreateInfo.Id);

            string y = null;
            var thisUrl = y;

                if(image == null){
                     thisUrl = "Empty";
                }else{

                     thisUrl = image.Url;

                }
                    


            return Ok(new
            {

                message = "Success",
                token = tokenHandler.WriteToken(token),
                User = new
                {

                    Username = createdUser.Username,
                    UserId = createdUser.Id,
                    UserType = userForRegisterDto.UserTypeId,
                    Firstname = creatdInfo.Firstname,
                    Middlename = creatdInfo.Middlename,
                    Lastname = creatdInfo.Lastname,
                    Phonenumber = createdUser.Phonenumber,
                    Image = thisUrl


                }

            });


        }



        [AllowAnonymous]
        [HttpPost("login")]

        public ActionResult Login(UserForLoginDto userForLoginDto)
        {

            // throw new Exception("test");
            int? busId = null;

            var userFromRepo =  AuthServices.Instance.Login(userForLoginDto.Phonenumber, userForLoginDto.Password, userForLoginDto.DeviceToken);


            if (userFromRepo == null)
                return Unauthorized(new { message = "Unauthorized" });

            if(userFromRepo.IsLogginedIn == 1)
                return Unauthorized(new { message = "Already Logged In" });

            if(userFromRepo.UserTypeId != 1)
            {
            userFromRepo.IsLogginedIn = 1;
            _context.Update(userFromRepo);
             _context.SaveChanges();
            }

            var userInfoFromRepo =  AuthServices.Instance.GetUserInfo(userFromRepo.Id);

            var IsDriver =  AuthServices.Instance.HaveBus(userFromRepo.Id);
            if (IsDriver != null)
            {
                busId = IsDriver.Id;
            }


            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, userFromRepo.Id.ToString()),
                new Claim(ClaimTypes.Name, userFromRepo.Username)

            };

            var key = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(_config.GetSection("AppSettings:Token").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = System.DateTime.Now.AddDays(365),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDescriptor);

            var image =  PhotoServices.Instance.getUserImage("User", userFromRepo.Id);
            string y = null;
            var thisUrl = y;

            if(image == null){
                     thisUrl = "Empty";
                }else{

                     thisUrl = image.Url;

                }





            return Ok(new
            {
                message = "Success",
                token = tokenHandler.WriteToken(token),
                User = new
                {

                    Username = userFromRepo.Username,
                    UserId = userFromRepo.Id,
                    UserType = userFromRepo.UserTypeId,
                    Firstname = userInfoFromRepo.Firstname,
                    Middlename = userInfoFromRepo.Middlename,
                    Lastname = userInfoFromRepo.Lastname,
                    Phonenumber = userFromRepo.Phonenumber,
                    BusId = busId,
                    Image = thisUrl
                }

            });
        }

        [HttpPost("EditProfile")]

        public ActionResult EditProfile(EditProfileDto editProfileDto)
        {

            string Url;

            var userToUpdateInfo = new UserInfo
            {
                Firstname = editProfileDto.Firstname,

                Middlename = editProfileDto.Middlename,

                Lastname = editProfileDto.Lastname,

                UserId = editProfileDto.UserId

            };

            var userEditedInfo =  AuthServices.Instance.EditProfile(userToUpdateInfo);

            var userImage =  PhotoServices.Instance.getUserImage("User", userEditedInfo.UserId);
                if(userImage == null)
                 {
                         Url = null;
                 }else{

                         Url = userImage.Url;
                 }   

            if (userEditedInfo == null)
                return Unauthorized(new { message = "Unauthorized" });

            return Ok(new
            {
                message = "Success",
                User = new
                {

                    Firstname = userToUpdateInfo.Firstname,
                    Middlename = userToUpdateInfo.Middlename,
                    Lastname = userToUpdateInfo.Lastname,
                    Url = Url

                }

            });
        }

        [HttpPost("ChangePassword")]

        public ActionResult ChangePassword(PasswordsDto passwordsDto)
        {

            var userFromRepo =  AuthServices.Instance.ChangePassword(passwordsDto);

            if (userFromRepo == null)
                return Unauthorized(new { message = "Unauthorized" });

            return Ok(new { message = "Success" });
        }

        [HttpPost("UpdateAttendanceStatus")]

        public ActionResult UpdateAttendanceStatus(AttendanceDto attendanceDto)
        {

            var passenger =  AuthServices.Instance.updateAttendanceStatus(attendanceDto);
            if(passenger == null)
                return BadRequest(new { message = "No Children with this Id is assigned to a bus"});



            return Ok(new { message = "Success" });
        }

        [AllowAnonymous]
        [HttpPost("PhonenumberValidation")]

        public ActionResult PhonenumberValidation(ValidationDto validationDto)
        {

            var user =  AuthServices.Instance.UserExists(validationDto.Phonenumber);
            if(user == false)
                return Ok(new { message = "Phonenumber Doesnt Exisit"});



            return Ok(new { message = "Phonenumber Exisit" });
        }

        [AllowAnonymous]
        [HttpPost("Logout")]

        public ActionResult Logout(CommonIdDto commonIdDto)
        {

            var user =  AuthServices.Instance.Logout(commonIdDto.Id);
            if(user == null)
                return BadRequest(new { message = "User Doesnt Exisit"});



            return Ok(new { message = "Success" });
        }

        [HttpPost("ChangePhonenumber")]

        public ActionResult ChangePhonenumber(ChangePhonenumberDto changePhonenumber)
        {

            var user =  AuthServices.Instance.ChangePhonenumber(changePhonenumber.Id, changePhonenumber.Phonenumber);
            if(user == null)
                return BadRequest(new { message = "User Doesnt Exisit"});



            return Ok(new { message = "Success" });
        }

        [AllowAnonymous]
        [HttpPost("ForgetPassword")]

        public ActionResult ForgetPassword(ForgetPasswordDto forgetPasswordDto)
        {

            var user =  AuthServices.Instance.ForgetPassword(forgetPasswordDto.Phonenumber, forgetPasswordDto.Password);
            if(user == null)
                return BadRequest(new { message = "User Doesnt Exisit"});



            return Ok(new { message = "Success" });
        }

        [AllowAnonymous]
        [HttpGet("GetUserAvatar/{id}")]

        public ActionResult UserAvatar(int id)
        {

            var image =  PhotoServices.Instance.getUserImage("User", id);
            if(image == null)
                return BadRequest(new { message = "No Image"});



            return Ok(image);
        }
        

        [AllowAnonymous]
        [HttpPost("uploadAvatar")]

        public async Task<IActionResult> uploadAvatar(IFormFile image, [FromForm] string Type,[FromForm] int userId)
        {




            if(Type == null || Type == "")
                    return BadRequest(new { message = "Type is empty"});
                    
            if (userId == 0)
                    return BadRequest(new { message = "UserId is empty" });

            Photo imageToAdd = new Photo();

             if (image == null)
            {
                    imageToAdd.Type = Type;
                    imageToAdd.ImageFor = userId;
                    imageToAdd.Url = "http://www.hafelaty.com/Resources/Avatars/player.jpg";


                    if ( PhotoServices.Instance.isAvatarExisit(Type, userId))
                    {
                            var avatar =  PhotoServices.Instance.uploadAvatar(imageToAdd, "edit");
                            return Ok(avatar);


                    }
                    else
                    {
                            var avatar =  PhotoServices.Instance.uploadAvatar(imageToAdd, "add");
                            return Ok(avatar);


                    }

            }else{
            

                    var folderName = Path.Combine("Resources", "Avatars");
                    var filePath = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                    string extension = System.IO.Path.GetExtension(image.FileName);

                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }

                    string nameToUse = Type;
                    nameToUse = nameToUse.Replace(" ", String.Empty);


                    var uniqueFileName = $"{nameToUse}_avatar_{userId}{extension}";
                    var dbPath = Path.Combine(folderName, uniqueFileName);

                    using (var fileStream = new FileStream(Path.Combine(filePath, uniqueFileName), FileMode.Create))
                    {
                         await image.CopyToAsync(fileStream);
                    }

                    var Url = $"http://www.hafelaty.com/{dbPath}";

                    var replacedUrl = Url.Replace(@"\","/");


                    imageToAdd.Type = Type;
                    imageToAdd.ImageFor = userId;
                    imageToAdd.Url = replacedUrl;

                    if ( PhotoServices.Instance.isAvatarExisit(Type, userId))
                    {
                            var avatar =  PhotoServices.Instance.uploadAvatar(imageToAdd, "edit");
                            return Ok(avatar);


                    }
                    else
                    {
                            var avatar =  PhotoServices.Instance.uploadAvatar(imageToAdd, "add");
                            return Ok(avatar);


                    }

              }

            
        }

    }
}