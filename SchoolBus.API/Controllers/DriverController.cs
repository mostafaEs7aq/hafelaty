using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Firebase.Database;
using Firebase.Database.Query;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using HafelatyServices;
using HafelatyServices.Dtos;
using HafelatyServices.Dtos.ToReturnDto;
using HafelatyEntites;

namespace SchoolBus.API.Controllers
{

    // [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class DriverController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly DataContext _context;

        public DriverController(IMapper mapper, DataContext context)
        {
            _mapper = mapper;
            _context = context;

        }




        [HttpGet("ChildrensByBus/{id}/{status}")]
        public ActionResult ChildrensByBus(int Id, string status)
        {

            var mapped = new List<Object>();
            dynamic obj;
            string notifcation;
            string Url;

            var childrens =  DriverServices.Instance.ChildrensByBus(Id, status);
            if (childrens == null)
                return BadRequest(new { message = "No Childs For This Bus" });

            var updatedBus =  _context.Bus.FirstOrDefault(x => x.Id == Id);

            foreach (var elem in childrens)
            {

                string name = elem.Firstname + " " + elem.Lastname;
                var image =  PhotoServices.Instance.getUserImage("Children", elem.Id);
                if(status == "start1"){

                    var track =  AppServices.Instance.TrackChildren(elem.Id, 5);


                }else{

                    var track =  AppServices.Instance.TrackChildren(elem.Id, 3);

                }

                if (image == null)
                {

                    Url = null;

                }
                else
                {

                    Url = image.Url;

                }



                var parents =  _context.Parents.FirstOrDefault(x => x.Id == elem.ParentsId);
                var parentRecord =  _context.Users.FirstOrDefault(x => x.Id == parents.PrimaryParent);

                notifcation =  AuthServices.Instance.SendNotifications(elem.ParentsId, status, name, updatedBus.Id);

                if (updatedBus.Trip == 1)
                {

                    obj = new
                    {

                        id = elem.Id,
                        firstname = elem.Firstname,
                        middlename = elem.Middlename,
                        lastname = elem.Lastname,
                        Age = elem.Age,
                        lat = elem.StartLat,
                        lng = elem.StartLong,
                        address = elem.StartAddress,
                        schoolId = elem.SchoolId,
                        Phonenumber = parentRecord.Phonenumber,
                        url = Url

                    };

                }
                else if (updatedBus.Trip == 2)
                {

                    obj = new
                    {

                        id = elem.Id,
                        firstname = elem.Firstname,
                        middlename = elem.Middlename,
                        lastname = elem.Lastname,
                        Age = elem.Age,
                        lat = elem.EndLat,
                        lng = elem.EndLong,
                        address = elem.EndAddress,
                        schoolId = elem.SchoolId,
                        Phonenumber = parentRecord.Phonenumber,
                        url = Url


                    };
                }
                else
                {

                    obj = new
                    {

                        id = elem.Id,
                        firstname = elem.Firstname,
                        middlename = elem.Middlename,
                        lastname = elem.Lastname,
                        Age = elem.Age,
                        lat = elem.StartLat,
                        lng = elem.StartLong,
                        address = elem.StartAddress,
                        schoolId = elem.SchoolId,
                        Phonenumber = parentRecord.Phonenumber,
                        url = Url


                    };
                }


                mapped.Add(obj);

            };

            return Ok(mapped);

        }

        [HttpGet("SchoolsByBus/{id}")]
        public ActionResult SchoolsByBus(int Id)
        {

            var schools =  DriverServices.Instance.GetSchoolsByBus(Id);
            if (!schools.Any())
                return BadRequest(new { message = "No Schools For This Bus" });



            return Ok(schools);

        }

        [HttpGet("ChildrensBySchool/{busId}/{schoolId}")]
        public ActionResult GetChildrensBySchool(int busId, int schoolId)
        {

            var childrens =  DriverServices.Instance.GetChildrensBySchool(busId, schoolId);
            if (!childrens.Any())
                return BadRequest(new { message = "No Childrens For This School" });



            return Ok(childrens);

        }

        [HttpPost("childStatus")]
        public ActionResult ChildStatus(RecieveChildDto recieveChildDto)
        {
            int busId;
            var child =  DriverServices.Instance.RecieveChild(recieveChildDto);
            if (child == null)
                return BadRequest(new { message = "Error" });

            var passegners =  _context.Passengers.FirstOrDefault(x => x.ChildrenId == child.Id);
            if (passegners == null)
            {
                busId = 0;
            }
            else
            {
                busId = passegners.BusId;
            }

            string sentStatus;
            if (recieveChildDto.Status == 0)
            {
                sentStatus = "with";
            }
            else
            {
                sentStatus = "absent";

            }

            string name = child.Firstname + " " + child.Lastname;


            var notifcation =  AuthServices.Instance.SendNotifications(child.ParentsId, sentStatus, name, busId);




            return Ok(new { message = "Done" });

        }

        [HttpPost("busArrived")]
        public ActionResult BusArrived(CommonIdDto commonIdDto)
        {
            int busId;
            var parents =  _context.Childrens.FirstOrDefault(x => x.Id == commonIdDto.Id);
            if (parents == null)
                return BadRequest(new { message = "Error" });

            string name = parents.Firstname + " " + parents.Lastname;

            var passegners =  _context.Passengers.FirstOrDefault(x => x.ChildrenId == commonIdDto.Id);
            if (passegners == null)
            {
                busId = 0;
            }
            else
            {
                busId = passegners.BusId;
            }


            var notifcation =  AuthServices.Instance.SendNotifications(parents.ParentsId, "arrived", name, busId);


            return Ok(new { message = "Done" });

        }

        [HttpGet("getEmergencyCases/{type}")]
        public ActionResult GetEmergencyCases(string type)
        {
            var emergency =  DriverServices.Instance.GetEmergencyCases(type);
            if (emergency == null)
                return BadRequest();

            return Ok(emergency);

        }



        [HttpPost("sendEmergency")]
        public ActionResult SendEmergency(EmergencyDto emergencyDto)
        {
            var emergency =  DriverServices.Instance.SendEmergency(emergencyDto);
            if (emergency == null)
                return BadRequest(new { message = "Something Went Wrong!" });

            return Ok(new { message = "Success" });

        }

        [HttpPost("recieveChildrensStatus")]
        public ActionResult RecieveChildrensStatus(test recieved)
        {

            var json = JsonConvert.DeserializeObject<List<request>>(recieved.testString);

            foreach (var obj in json)
            {
                var children =  _context.Childrens.FirstOrDefault(x => x.Id == obj.i);
                string childName = children.Firstname + " " + children.Lastname;
                var track =  AppServices.Instance.TrackChildren(children.Id, 2);

                if (obj.t == "checked")
                {

                    var notifcation =  AuthServices.Instance.SendNotifications(children.ParentsId, "absent", childName, null);

                }
                else
                {

                    var notifcation =  AuthServices.Instance.SendNotifications(children.ParentsId, "with", childName, null);

                }


            };

            return Ok(new { message = "Success" });



        }










    }
}