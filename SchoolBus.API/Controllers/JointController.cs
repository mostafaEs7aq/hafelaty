using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Linq;
using AutoMapper;
using System.Collections.Generic;
using HafelatyServices;
using HafelatyServices.Dtos;
using HafelatyServices.Dtos.ToReturnDto;
using HafelatyEntites;

namespace SchoolBus.API.Controllers
{

    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class JointController : ControllerBase
    {
        private readonly IMapper _mapper;
        public JointController(IMapper mapper)
        {
            _mapper = mapper;
        }


        [AllowAnonymous]
        [HttpGet("getschools")]

        public ActionResult GetSchools()
        {

            var schools =  JointServices.Instance.GetSchools();

            var schoolsToReturn = _mapper.Map<IEnumerable<SchoolsToReturnDto>>(schools);

            return Ok(schoolsToReturn);

        }

        [AllowAnonymous]
        [HttpGet("getschool/{id}")]

        public ActionResult GetSchool(int id)
        {


            var school =  JointServices.Instance.GetSchool(id);

            var schoolToReturn = _mapper.Map<SchoolsToReturnDto>(school);


            return Ok(schoolToReturn);

        }

    }

}





