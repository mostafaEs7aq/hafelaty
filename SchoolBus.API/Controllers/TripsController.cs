using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Linq;
using AutoMapper;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System.IO;
using Newtonsoft.Json;
using HafelatyServices;
using HafelatyServices.Dtos;
using HafelatyServices.Dtos.ToReturnDto;
using HafelatyEntites;

namespace SchoolBus.API.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class TripsController : ControllerBase
    {
        private readonly IMapper _mapper;
        public TripsController(IMapper mapper)
        {
            _mapper = mapper;
        }


        // [AllowAnonymous]
        [HttpGet("getRoundTimes")]

        public ActionResult GetRoundTimes()
        {
            var times =  TripServices.Instance.GetRoundTimes();
            if(times == null)
                return BadRequest(new {message = "No Times Available"});

            var timesToReturn = _mapper.Map<IEnumerable<TimesToReturnDto>>(times);


            return Ok(timesToReturn);

        }

        [HttpGet("getRoundTypes")]

        public ActionResult GetRoundTypes()
        {
            var types =  TripServices.Instance.GetRoundTypes();
            if(types == null)
                return BadRequest(new {message = "No Types Available"});

            var typesToReturn = _mapper.Map<IEnumerable<TypesToReturnDto>>(types);


            return Ok(typesToReturn);

        }

        [HttpGet("getAllBusesRounds")]

        public ActionResult GetAllBusesRounds()
        {
            var allBusRounds =  TripServices.Instance.GetAllBusesRounds();
            if(allBusRounds == null)
                return BadRequest(new {message = "No Rounds Available"});


            return Ok(allBusRounds);

        }

        [HttpGet("getBusRounds/{busId}")]

        public ActionResult GetBusRounds(int busId)
        {
            var busRounds =  TripServices.Instance.GetBusRounds(busId);
            if(!busRounds.Any())
                return BadRequest(new {message = "No Rounds Available"});


            return Ok(busRounds);

        }

        [HttpGet("getBusRoundsByTime/{busId}/{timeId}")]

        public ActionResult GetBusRoundsByTime(int busId, int timeId)
        {
            var busRounds =  TripServices.Instance.GetBusRoundsByTime(busId, timeId);
            if(!busRounds.Any())
                return BadRequest(new {message = "No Rounds Available"});


            return Ok(busRounds);

        }

        [HttpGet("getBusRoundsByType/{busId}/{typeId}")]

        public ActionResult GetBusRoundsByType(int busId, int typeId)
        {
            var busRounds =  TripServices.Instance.GetBusRoundsByType(busId, typeId);
            if(!busRounds.Any())
                return BadRequest(new {message = "No Rounds Available"});


            return Ok(busRounds);

        }

        [HttpPost("addBusRound")]

        public ActionResult AddBusRound(BusRoundDto busRoundDto)
        {
            var busToAdd =  TripServices.Instance.AddBusRound(busRoundDto);

            return Ok(busToAdd);

        }

        [HttpPost("editBusRound")]

        public ActionResult EditBusRound(BusRoundToEditDto busRoundDto)
        {
            var busToEdit =  TripServices.Instance.EditBusRound(busRoundDto);

            return Ok(busToEdit);

        }

        [HttpPost("deleteBusRound")]

        public ActionResult DeleteBusRound(CommonIdDto commonIdDto)
        {
            var busToDelete =  TripServices.Instance.DeleteBusRound(commonIdDto);
            if(!busToDelete)
                return BadRequest(new { message = "Something Went Wrong"});

            return Ok(new { message = "Success"});

        }

        [HttpPost("addRoundPassenger")]

        public ActionResult AddRoundPassenger(RoundPassengerDto roundPassengerDto)
        {
            var passengerToAdd =  TripServices.Instance.AddRoundPassenger(roundPassengerDto);
            if(!passengerToAdd)
                return BadRequest(new { message = "Something Went Wrong"});

            return Ok(new { message = "Success"});

        }

        [HttpPost("deleteRoundPassenger")]

        public ActionResult DeleteRoundPassenger(RoundPassengerDto roundPassengerDto)
        {
            var passengerToDelete =  TripServices.Instance.DeleteRoundPassenger(roundPassengerDto);
            if(!passengerToDelete)
                return BadRequest(new { message = "Something Went Wrong"});

            return Ok(new { message = "Success"});

        }

        [HttpGet("getRoundPassengers/{roundId}")]

        public ActionResult GetRoundPassengers(int roundId)
        {
            var passengers =  TripServices.Instance.GetRoundPassengers(roundId);
            if(!passengers.Any())
                return BadRequest(new { message = "No Passengers Yet"});
            
            var round =  TripServices.Instance.GetRoundRecord(roundId);


            return Ok(passengers);

        }

        [HttpGet("getBuses")]

        public ActionResult GetBuses()
        {
            var buses =  TripServices.Instance.GetBuses();
            if(!buses.Any())
                return BadRequest(new { message = "No Buses Yet"});

            return Ok(buses);

        }

        [HttpGet("getChildrens")]

        public ActionResult GetChildrens()
        {
            var childrens =  TripServices.Instance.GetChildrens();
            if(!childrens.Any())
                return BadRequest(new { message = "No Childrens Yet"});

            return Ok(childrens);

        }

        [HttpGet("getSchoolsByRound/{roundId}")]

        public ActionResult GetSchoolsByRound(int roundId)
        {
            var schools =  TripServices.Instance.GetSchoolsByRound(roundId);
            if(!schools.Any())
                return BadRequest(new { message = "No Schools Yet"});

            return Ok(schools);

        }

        [HttpPost("recieveChildrensStatus")]
        public ActionResult RecieveChildrensStatus(test recieved)
        {

            var json = JsonConvert.DeserializeObject<List<request>>(recieved.testString);
            if( TripServices.Instance.HandleChildrenStatus(json))
            {
                return Ok(new { message = "Success" });
            }else{
                return BadRequest(new { message = "Something Wrong Happened" });
            }

        }

        [HttpPost("endTrip")]
        public ActionResult EndTrip(CommonIdDto commonIdDto)
        {

            if(! TripServices.Instance.HandleBusTrip(commonIdDto.Id, "End"))
                return BadRequest(new { message = "Something Wrong Happened" });

            return Ok(new { message = "Success" });

        }

        [HttpPost("startTrip")]
        public ActionResult StartTrip(CommonIdDto commonIdDto)
        {

            if(! TripServices.Instance.HandleBusTrip(commonIdDto.Id, "Start"))
                return BadRequest(new { message = "Something Wrong Happened" });

            return Ok(new { message = "Success" });

        }

        [HttpGet("childrensBySchool/{roundId}/{schoolId}")]
        public ActionResult GetChildrensBySchool(int roundId, int schoolId)
        {

            var childrens =  TripServices.Instance.GetChildrensBySchool(roundId, schoolId);
            if (!childrens.Any())
                return BadRequest(new { message = "No Childrens For This School" });

            return Ok(childrens);

        }

        [HttpGet("viewAllRoundPassengers/{roundId}")]
        public ActionResult viewAllRoundPassengers(int roundId)
        {

            var passengers =  TripServices.Instance.ViewAllRoundPassengers(roundId);
            if (!passengers.Any())
                return BadRequest(new { message = "No Passengers For This Round" });

            return Ok(passengers);

        }


    }

}





