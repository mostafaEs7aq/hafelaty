using AutoMapper;
using HafelatyEntites;
using HafelatyServices.Dtos.ToReturnDto;
using System.Collections.Generic;
using System.Linq;

namespace SchoolBus.API.Helpers
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {

            CreateMap<Schools, SchoolsToReturnDto>().ReverseMap();
            CreateMap<Childrens, ChildToReturnDto>().ReverseMap();
            CreateMap<Childrens, StartTripToReturnDto>().ReverseMap();
            CreateMap<List<Childrens>, List<ChildToReturnDto>>().ReverseMap();
            CreateMap<List<Childrens>, List<StartTripToReturnDto>>().ReverseMap();
            CreateMap<Bus, BusesToReturn>().ReverseMap();
            CreateMap<List<Bus>, List<BusesToReturn>>().ReverseMap();
            CreateMap<List<Bus>, List<BusesAmountsToReturn>>().ReverseMap();
            CreateMap<List<RoundTimes>, List<TimesToReturnDto>>().ReverseMap();
            CreateMap<RoundTimes, TimesToReturnDto>().ReverseMap();
            CreateMap<List<RoundTypes>, List<TypesToReturnDto>>().ReverseMap();
            CreateMap<RoundTypes, TypesToReturnDto>().ReverseMap();
            CreateMap<List<BusRound>, List<BusRoundsToReturnDto>>().ReverseMap();
            CreateMap<BusRound, BusRoundsToReturnDto>().ReverseMap();
            CreateMap<Bus, BusesAmountsToReturn>().ReverseMap();
            CreateMap<Verification, VouchersToReturn>().ReverseMap();
            CreateMap<List<Verification>, List<VouchersToReturn>>().ReverseMap();
            // CreateMap<User, UserToReturnDto>()
            //     .ForMember(dest => dest.PhotoUrl, opt => {
            //         opt.MapFrom(src => src.Photos.FirstOrDefault().Url);
            //     })
            //     .ForMember(dest => dest.Info, opt => {
            //         opt.MapFrom(src => src.Info.FirstOrDefault());
            //     });
            CreateMap<List<User>, List<DriverToReturnDto>>().ReverseMap();
            CreateMap<User, DriverToReturnDto>().ReverseMap();
            CreateMap<UserInfo, UserInfoToReturnDto>().ReverseMap(); // add the user info and photo
            CreateMap<Photo, PhotoToReturnDto>().ReverseMap(); // add the user info and photo
            CreateMap<List<Schools>, List<SchoolsToReturnDto>>().ReverseMap();
            CreateMap<List<User>, List<AdminsToReturnDto>>().ReverseMap();
        }
    }
}