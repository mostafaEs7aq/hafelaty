<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/7.5.0/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->

<script>
  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyCuGBvS2HwCw35cX6dribcCkxXh-wQicIQ",
    authDomain: "joschool-db5fe.firebaseapp.com",
    databaseURL: "https://joschool-db5fe.firebaseio.com",
    projectId: "joschool-db5fe",
    storageBucket: "joschool-db5fe.appspot.com",
    messagingSenderId: "332063872203",
    appId: "1:332063872203:web:45e20a57c278d875fb774e"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
</script>